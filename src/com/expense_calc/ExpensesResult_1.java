package com.expense_calc;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Window;

import com.application.tripsmart.R;
import com.custom_dialogs.ProgressDialog_1;
import com.data_classes.GasDataClass;
import com.data_classes.UserData;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.notice.Issue;
import com.notice.Issue_Network;
import com.utility.Comparable;
import com.utility.DecoderClass;
import com.utility.DeviationCalculator;
import com.utility.DistanceCalculator;
import com.utility.GasUtility_v2;
import com.utility.PreViewOperations;
import com.utility.StreamToString;

public class ExpensesResult_1 extends Activity 
{
	Context context;
	String start,destination;
	ArrayList<UserData> data1;
	String waypoints = "";
	ArrayList<String> waypoints1;
	String result;
	ArrayList<LatLng> route_map;
	String test;
	double mileage;
	double distance;
	double gas_quantity;
//	ArrayList<Rounds> round;
//	ArrayList<Checkpoint> checkpoint;
	ArrayList<String> checkpoint_loc_name;
	ArrayList<LatLng> checkpoint_loc;
	ArrayList<GasDataClass> gas_data;
	Geocoder gc;
	
	//Improved Code Variables
	int user_checkpoint_count = -1;
	int gas_points_count = -1;
	double mileage_1gallon;// = 20;
	ArrayList<LatLng> gas_points;
	double minus_value = 30;
	ArrayList<GasDataClass> gas_station_list;
	//ArrayList<CheckPoint_new> checkpoint_new;
	double price, tank;
	
	String fuel_type;// = "reg";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		PreViewOperations pvo = new PreViewOperations(ExpensesResult_1.this, R.layout.activity_expenses_result_1, R.drawable.background_expense_calc);
		
		super.onCreate(savedInstanceState);
		setContentView(pvo.returnView());
		
//		super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_expenses_result_1);
		
		context = ExpensesResult_1.this;
		Bundle extras = getIntent().getExtras();
		start = extras.getString("start");
		destination = extras.getString("destination");
		tank = extras.getDouble("tank",1);
		fuel_type = extras.getString("fuel_type");
		waypoints1 = new ArrayList<String>();
		mileage_1gallon = extras.getDouble("mileage");
		mileage = tank * mileage_1gallon;//extras.getDouble("mileage");//change
		checkpoint_loc_name = new ArrayList<String>();
		checkpoint_loc = new ArrayList<LatLng>();
		minus_value = 15/100.0 * mileage;
		gc = new Geocoder(ExpensesResult_1.this);
		try 
		{
			JSONArray input = new JSONArray(extras.getString("user_json"));
			//JSONObject jo = input.getJSONObject(0);
			UserData data;
			data1 = new ArrayList<UserData>();
			for(int i=0;i<input.length()-1;i++)
			{
				data = null;
				data = new UserData(input.getJSONObject(i).getString("name"),input.getJSONObject(i).getString("loc"),0);
				if(data!=null)
				{
					data1.add(data);
				}
				if(!(data.loc.equals(destination)))
				{
					waypoints = waypoints + data.loc +"|";
					waypoints1.add(data.loc);
				}
			}
			RetrieveData rd = new RetrieveData();
			rd.execute();
		}
		catch (Exception e) 
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(context);
			alert.setMessage(e.toString());
			alert.show();
		}
	}
	
	class RetrieveData extends AsyncTask<String, String, Void>
	{
		ProgressDialog_1 prog;
		@Override
		protected void onPreExecute() 
		{
			Display dp = getWindowManager().getDefaultDisplay();
			prog = new ProgressDialog_1(context,"Working on It",dp.getWidth(),dp.getHeight());
			prog.show();
		}
		
		@Override
		protected Void doInBackground(String... params) 
		{
			double total_distance = 0;
			route_map = new ArrayList<LatLng>();
			HttpClient httpclient = new DefaultHttpClient();
			String str = "http://maps.googleapis.com/maps/api/directions/json?origin="+start.replace(" ", "%20")+"&destination="+destination.replace(" ", "%20")+"&region=en&sensor=true&waypoints=optimize:true|"+waypoints.replace(" ", "%20");
			try 
			{
				str = str.replace("|", "%7C");
				URLEncoder.encode(str, "UTF-8");
			} 
			catch (UnsupportedEncodingException e1) 
			{
				e1.printStackTrace();
			}
			HttpGet httpget = new HttpGet(str);
			HttpResponse response;
			try
			{
				response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				if(entity!=null)
				{
					InputStream instream = entity.getContent();
					StreamToString st = new StreamToString();
					result = st.convertStreamtoString(instream);
					JSONObject jobject = new JSONObject(result);
					JSONArray routes = (JSONArray)jobject.get("routes");
					JSONArray legs = (JSONArray)((JSONObject)routes.get(0)).get("legs");
					
					double distance_1 = 0;
					double distance_2 = 0;
					double check = mileage - minus_value;		//Instead of our regular constant value 30
					double sample_mileage = check;
					
					//Looping Scenario2 Adarsh Notebook
					try
					{
						
						gas_points = new ArrayList<LatLng>();
						DecoderClass dc = new DecoderClass();
						//distance_2  = 0;
						distance_2  = total_distance;
						for(int i=0;i<legs.length();i++)
						{
							JSONObject distance_value = (JSONObject)((JSONObject)legs.get(i)).get("distance");
							distance = distance + distance_value.getInt("value")*0.000621371;
							distance_1 = distance_2;
							distance_2 = distance_2 + distance_value.getInt("value")*0.000621371;
							
							//Check for mileage reached in current leg 
							if(distance_2>=sample_mileage)
							{
								JSONArray steps = (JSONArray)((JSONObject)legs.get(i)).get("steps");
								double steps_distance_1 = 0;
								double steps_distance_2 = distance_1;
								for(int j=0;j<steps.length();j++)
								{
									JSONObject polyline = (JSONObject)((JSONObject)steps.get(j)).get("polyline");
									JSONObject step_distance_value = (JSONObject)((JSONObject)steps.get(j)).get("distance");
									steps_distance_1 = steps_distance_2;
									steps_distance_2 =  steps_distance_2 + step_distance_value.getInt("value")*0.000621371;
									
									if(steps_distance_2 >= sample_mileage)
									{
										ArrayList<LatLng> points = dc.decodePoly(polyline.getString("points"));
										double points_distance_1 = 0;
										double points_distance_2 = steps_distance_1;
										DistanceCalculator dcalc = new DistanceCalculator();
										LatLng temp1;
										LatLng temp2;
										//double needed  = 0;
										for(int k=1;k<points.size();k++)
										{
											points_distance_1 = points_distance_2;
											temp1 = points.get(k-1);
											temp2 = points.get(k);
											points_distance_2 = points_distance_2 + dcalc.distanceCalc(temp1.latitude, temp1.longitude, temp2.latitude, temp2.longitude, "M");
											if(points_distance_2>sample_mileage)
											{
												gas_points.add(temp1);
												gas_points_count++;
												sample_mileage = sample_mileage + check;
											}
											//total_distance = points_distance_1;
										}
									}
								}
								Log.i("distance2", distance_2+"");
							}
						}
						Log.i("mileage", mileage+"");
						Log.i("check value", check+"");
						Log.i("gas_points", gas_points.size()+"");
						for (LatLng element : gas_points) 
						{
							Log.i("gas_points", element.toString());
						}
						//By the time you reach here, you will have the points where the gas is to be calculated.
						//Calculate gas stations for that point and then fill out the deviation too.
//						ArrayList<GasDataClass> result;
						
						//Add the start point as a gas point too.
						Geocoder geo = new Geocoder(context);
						List<Address> temp = geo.getFromLocationName(start, 1);
						gas_points.add(new LatLng(temp.get(0).getLatitude(), temp.get(0).getLongitude()));
						
						if(gas_points.size()>0)
						{
//							GasUtility_v2 gu = new GasUtility_v2(context, gas_points.get(0), 5, "reg", "distance");
//							result = gu.returnData();
							
							gas_station_list = new ArrayList<GasDataClass>();
							
							//My Algorithm for getting the best gas data.
							//First get some good gas stations.
							//Take best 10 gas stations(least distance) and best 10 gas stations (least price) and take the common ones of them and put them in a list such that least distance order can be seen.
							//Take the first gas station from the list and calculate the deviation(distance from this gas point to gas station + distance from gas station to next gas point).
							//If deviation is greater that sample mileage, then skip the gas station.
							//If deviations is lesser, calculate total price for that deviation taking in account the price of the gas at gas station.
							//Repeat the same process for other gas stations.
							//Finally take the gas station with the least price.
							int i = 0;
							ArrayList<LatLng> gas_stations;
							gas_stations = new ArrayList<LatLng>();
							Log.i("mileage", mileage+"");
							Log.i("check value", check+"");
							Log.i("gas_points", gas_points.get(i).toString());
							while(true)
							{
								 
								ArrayList<GasDataClass> best_distance;
								ArrayList<GasDataClass> best_price;
								ArrayList<GasDataClass> best_best = new ArrayList<GasDataClass>();
								ArrayList<Integer> remove_points = new ArrayList<Integer>();
								
								LatLng point1 = gas_points.get(i);
								LatLng point3;
								
								GasUtility_v2 gu = new GasUtility_v2(ExpensesResult_1.this, point1, 40, fuel_type, "distance"); //Use percentage
								best_distance =  gu.returnData();
								
								GasUtility_v2 gu1 = new GasUtility_v2(ExpensesResult_1.this, point1, 40, fuel_type, "price");		//Use percentage
								best_price = gu.returnData();
								
								int j = 0;
								int k = 0;
								while(true)
								{
									GasDataClass tmp  = best_distance.get(j);
									while(true)
									{
										GasDataClass tmp1 = best_price.get(k);
										
										if(tmp.address.equals(tmp1.address))
										{
											best_best.add(tmp);
										}
										k++;
										if(k==6)
										{
											k = 0;
											break;
										}
									}
									j++;
									if(j==6)
									{
										break;
									}
								}
								
								Log.i("no of gas_points", best_best.size()+"");
								
								//LatLng point2 = 
								if(i==(gas_points.size()-1))
								{
//									String[] temp = destination.split(",");
									Geocoder gc = new Geocoder(ExpensesResult_1.this);
									List<Address> addr_temp = gc.getFromLocationName(destination, 1);
									Address address = addr_temp.get(0);
									point3  = new LatLng(address.getLatitude(), address.getLongitude());
								}
								else
								{
									point3 = gas_points.get(i+1); 
								}
								
								//Calculate distance here, we get two distances distance from point1 to point2 and distance from point2 to point3
								for(int x=0;x<best_best.size();x++)
								{
									GasDataClass tmp = best_best.get(x);
									try
									{
										
										DeviationCalculator devc = new DeviationCalculator(ExpensesResult_1.this);
										best_best.get(x).deviation = devc.returnData(tmp.location,point1,point3);
										best_best.get(x).total_cost = (tmp.deviation/mileage)*tmp.price;
										
										if(best_best.get(x).deviation>check)
										{
											remove_points.add(x);
										}
									}
									catch (Exception e)
									{
										Log.i("dafaq", e.toString());
									}
									
									Log.i("testing_Deviation", best_best.get(x).deviation+"");
									Log.i("testing_price", best_best.get(x).price+"");
								}
								for (Integer integer : remove_points) 
								{
									best_best.remove(integer);
								}
								
								//Insert the best gas station into the list
								Collections.sort(best_best, new Comparable("total_cost"));
								gas_station_list.add(best_best.get(0));

								if(best_best.size()==0)
								{
									Log.i("seems to be dead", "nothing much to say");
								}
								
								//Clear the best list
								best_distance.clear();
								best_price.clear();
								best_best.clear();
								remove_points.clear();
								//point1 to point2 comes under the previous gas station's price
								//point2 to point3 comes under the current gas staions's price
								
								i++;
								Log.i("total points", gas_points_count+"");
								Log.i("value of i", ""+i);
								if(i==gas_points.size())
								{
									//Break out of the loop when we reach the end of the gas_stop points list
									break;
								}
							}							
						}
					}
					catch (ArrayIndexOutOfBoundsException e) 
					{
						Log.i("loop2.a", e.toString());
						Log.i("loop2.a", e.toString());
					}
					catch (Exception e)
					{
						Log.i("loop2", e.toString());
					}
				}
			}
			catch (UnknownHostException e) 
			{
				context.startActivity(new Intent(context, Issue_Network.class));
				((Activity)context).finish();
			}
			catch(Exception e)
			{
				Log.i("myexception", e.toString());
				context.startActivity(new Intent(context, Issue.class));
				((Activity)context).finish();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result)
		{
			prog.dismiss();
			  
			//All data sent to the result activity
			Gson gson = new Gson();
			Intent intent = new Intent(ExpensesResult_1.this,ExpensesResult_2.class);
			intent.putExtra("gas_stations", gson.toJson(gas_station_list));
			intent.putExtra("start", start);
			intent.putExtra("mileage_1_gallon", mileage_1gallon);
			intent.putExtra("tank", tank);
			intent.putExtra("destination", destination);
			intent.putExtra("userdata", gson.toJson(data1));
			intent.putExtra("waypoints", waypoints1);
			context.startActivity(intent);
			finish();
			overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		}
	}
}
