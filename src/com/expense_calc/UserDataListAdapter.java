package com.expense_calc;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.application.tripsmart.R;
import com.data_classes.UserData;

public class UserDataListAdapter extends ArrayAdapter<UserData> 
{
	ArrayList<UserData> objects;
	Context context;
	
	public UserDataListAdapter(Context context, int textViewResourceId, ArrayList<UserData> objects) 
	{
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.context = context;
	}

	@Override
	public View getView(final int position, View convertView,final ViewGroup parent)
	{
		View rowview = convertView;
		
		if(rowview == null)
		{
			ViewHolder viewholder = new ViewHolder();
			final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowview = inflater.inflate(R.layout.list_result_expenses_user, parent, false);
			viewholder.tv0 = (TextView)rowview.findViewById(R.id.textView1);
			viewholder.rowview = rowview;
			rowview.setTag(viewholder);
		}
		
		ViewHolder holder = (ViewHolder) rowview.getTag();
		final DecimalFormat df = new DecimalFormat("##.##");
		holder.tv0.setText(objects.get(position).name+" has to pay "+"$"+df.format(objects.get(position).pay)+" for the trip.");
		Log.i("The position is",position+"");
		
		holder.rowview.setOnClickListener(new View.OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				UserData data = objects.get(position);
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("smsto:"+data.phno));
				intent.putExtra("sms_body","We have travelled together and you need to pay your share of "+df.format(data.pay)+" for the trip.");
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
				Activity act = (Activity)context;
				act.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
			}
		});
		return rowview;
	}
	
	static class ViewHolder
	{
		TextView tv0;
		View rowview;
	}
}
