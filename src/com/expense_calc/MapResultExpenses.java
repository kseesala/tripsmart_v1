package com.expense_calc;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.custom_dialogs.ProgressDialog_1;
import com.data_classes.LocationData;
import com.data_classes.UserData;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.utility.Check_Internet;
import com.utility.DecoderClass;
import com.utility.StreamToString;

public class MapResultExpenses extends FragmentActivity implements LocationListener 
{
	private GoogleMap map;
	private PolylineOptions polyOptions;	
	private String provider;
	private Polyline polyline;
	private ArrayList<LocationData> locdata;
	private ArrayList<UserData> userdata;
	private LatLng loc_temp;
	boolean enabled = false;
	boolean checked = false;
	
	Check_Internet ic;
	Gson gson;
	String waypoints = "";
	Bundle extras;
	
	Button b1;
	
	ArrayList<LatLng> polyline_points;
	
	String uri;
	
	LocationManager locm;
	
	boolean gps = false;
	boolean net = false;
	Location myloc = null;
	
	ProgressDialog_1 pd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_activity_expenses);
		extras = getIntent().getExtras();
		map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
//		gc = new Check_GPS(MapResultExpenses.this);
		ic = new Check_Internet(MapResultExpenses.this);
		gson = new Gson();
		uri = extras.getString("URI");
		locm  = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		
		checkerFunction();		
		
		if(checked!=false)
		{
			checkAndEnableListener();
			
			b1 = (Button)findViewById(R.id.start);
			
			b1.setOnClickListener(new View.OnClickListener() 
			{
				
				@Override
				public void onClick(View v) 
				{
					// TODO Auto-generated method stub
					moveToStart();
				}
			});
		}
		else
		{
			ic.gotoNetworkIssue();
		}
	}
	
	private void checkerFunction()
	{
		//Network Connection Check		
		try 
		{
			checked = ic.isNetworkAvailable();
		} 
		catch (Exception e1) 
		{
			Log.i("check1",e1.toString());
		}
	}
	
	protected void moveToStart()
	{
		CameraUpdate center = CameraUpdateFactory.newLatLng(locdata.get(0).loc);
		CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
		map.moveCamera(center);
		map.animateCamera(zoom);
	}
	
	protected void checkAndEnableListener()
	{
		
		if(pd == null)
		{
			Display dp = getWindowManager().getDefaultDisplay();
			pd = new ProgressDialog_1(MapResultExpenses.this, "Working on it", dp.getWidth(), dp.getHeight());
			pd.show();
		}
		else if(!pd.isShowing())
		{
			pd.show();
		}
		
		if(locm.isProviderEnabled(LocationManager.GPS_PROVIDER))
		{
			gps = true;
		}

		if(locm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
		{
			net = true;
		}
		
		if(gps == true)
		{
			locm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		}
		if(net == true)
		{
			locm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		}
	}	
	
	@Override
	public void onLocationChanged(Location location) 
	{
		// TODO Auto-generated method stub
		if(myloc == null)
		{
			myloc = location;
		}
		else
		{
			long temp = location.getTime() - myloc.getTime();
			
			if(temp > (1000*10))
			{
				myloc = location;
			}
			else if(location.getAccuracy() > myloc.getAccuracy())
			{
				myloc = location;
			}
		}
		
		doYourWork();
		locm.removeUpdates(this);
	}
	
	protected void doYourWork()
	{	
		map.setMyLocationEnabled(true);
		map.setIndoorEnabled(true);
		map.getUiSettings().setMyLocationButtonEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(true);
		map.getUiSettings().setZoomGesturesEnabled(true);
		map.getUiSettings().setAllGesturesEnabled(true);
		map.getUiSettings().setRotateGesturesEnabled(true);
		
		
		locdata = gson.fromJson(extras.getString("result"),new TypeToken<ArrayList<LocationData>>(){}.getType());
		userdata = gson.fromJson(extras.getString("result_user"),new TypeToken<ArrayList<UserData>>(){}.getType());
		polyline_points = new ArrayList<LatLng>();
		
		for (LocationData element : locdata) 
		{
			waypoints = waypoints + element.loc.latitude + "," + element.loc.longitude;
			MarkerOptions mk = new MarkerOptions().position(element.loc);
			if(element.tag.equals("gas_station"))
			{
				//Marker marker_temp = map.addMarker(new MarkerOptions().position(element.loc).title(element.name).snippet("$"+element.price));
				mk.title(element.comment);
				mk.snippet("$"+element.price);
			}
			else if(element.tag.equals("checkpoint"))
			{
				//Marker marker_temp = map.addMarker(new MarkerOptions().position(element.loc).title(element.name).snippet("$"+element.price).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
				mk.title(element.name).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
				mk.snippet(element.comment);
			}
			else
			{
				mk.title(element.name).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
			}
			Marker marker_temp = map.addMarker(mk);
		}
		
		moveToStart();	
		
		GetAllPoints gap = new GetAllPoints();
		gap.execute();
	}
	
	@Override
	public void onProviderDisabled(String provider) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) 
	{
		// TODO Auto-generated method stub
		
	}
	
	
	class GetAllPoints extends AsyncTask<Void, Void, Boolean>
	{

		@Override
		protected void onPreExecute()
		{
			if(!pd.isShowing())
			{
				pd.show();
			}
		}
		
		@Override
		protected Boolean doInBackground(Void... params) 
		{
			// TODO Auto-generated method stub
			String waypoints  = "";
			String uri = "http://maps.googleapis.com/maps/api/directions/json?origin=";			
			HttpClient httpclient = new DefaultHttpClient();
			String result = "";
			
			for(int i=1;i<locdata.size()-1;i++)
			{
				LatLng temp = locdata.get(i).loc;
				waypoints = waypoints + temp.latitude+","+temp.longitude+"|";
			}
			
			uri = uri + locdata.get(0).loc.latitude+","+locdata.get(0).loc.longitude;
			uri = uri + "&destination="+locdata.get(locdata.size()-1).loc.latitude+","+locdata.get(locdata.size()-1).loc.longitude;
			uri = uri + "&region=en&sensor=true&waypoints=";
			uri = uri + waypoints;			
		
			try
			{
				uri = uri.replace("|", "%7C");
				URLEncoder.encode(uri, "UTF-8");
			}
			catch (UnsupportedEncodingException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			HttpGet httpget = new HttpGet(uri);
			HttpResponse response;
			try
			{
				response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				if(entity!=null)
				{
					
					InputStream instream = entity.getContent();
					StreamToString st = new StreamToString();
					result = st.convertStreamtoString(instream);
					//Log.i("testing string dist", result);
					JSONObject jobject = new JSONObject(result);
					JSONArray routes = (JSONArray)jobject.get("routes");
					JSONArray legs = (JSONArray)((JSONObject)routes.get(0)).get("legs");
					
					DecoderClass dc = new DecoderClass();
					for(int i=0;i<legs.length();i++)
					{
						JSONArray steps = (JSONArray)((JSONObject)legs.get(i)).get("steps");
						for(int j=0;j<steps.length();j++)
						{
							JSONObject polyline = (JSONObject)((JSONObject)steps.get(j)).get("polyline");
							ArrayList<LatLng> points = dc.decodePoly(polyline.getString("points"));
							Log.i("points size", ""+points.size());
							polyline_points.addAll(points);
						}
					}
				}
			}
			catch (Exception e)
			{
				Log.i("fuckit", e.toString());
			}
			
			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean result)
		{
			pd.dismiss();
			if(result)
			{
				Log.i("polylinepoints",polyline_points.size()+"");
				polyOptions = new PolylineOptions().addAll(polyline_points);
				
				polyOptions.color(Color.RED);
				polyline = map.addPolyline(polyOptions);
			}
			else
			{
				Toast.makeText(MapResultExpenses.this, "Unable to draw the route, Please try again later.", Toast.LENGTH_LONG).show();
			}
		}
	}
}
