package com.expense_calc;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

//Adapter to enable auto complete feature for source and destination
public class AutocompleteAdapter extends ArrayAdapter<Address> implements Filterable
{
	Context context;
	LayoutInflater inflater;
	Geocoder geo;
	public AutocompleteAdapter(Context context, int layoutid) 
	{
		super(context, layoutid);
		this.context = context;
		inflater = LayoutInflater.from(context);
		geo = new Geocoder(context,context.getResources().getConfiguration().locale);
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) 
	{
		TextView tv0 = (TextView)inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
		tv0.setTextColor(Color.BLACK);
		tv0.setText(getFormattedAddress(getItem(position)));
		return tv0;
	}
	
	public String getFormattedAddress(Address address)
	{
		String str = "";
		for(int i=0;i<address.getMaxAddressLineIndex();i++)
		{
			str = str + address.getAddressLine(i);
			if(i!=(address.getMaxAddressLineIndex()-1))
			{
				str = str + ",";
			}
		}
		return str;
	}
	
	@Override
	public Filter getFilter() 
	{
		Filter filter = new Filter() 
		{
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) 
			{
				clear();
				for (Address addr : (ArrayList<Address>)results.values) 
				{
					add(addr);
				}
				if(results.count > 0)
				{
					notifyDataSetChanged();
				}
				else
				{
					notifyDataSetInvalidated();
				}
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) 
			{
				ArrayList<Address> addr_data = new ArrayList<Address>();
				if(constraint!=null)
				{
					try 
					{
						addr_data =  (ArrayList<Address>) geo.getFromLocationName((String)constraint, 5);
					}
					catch (IOException e) 
					{
						Log.i("myexception", e.toString());
					}
				}
				final FilterResults results = new FilterResults();
				results.values = addr_data;
				results.count = addr_data.size();
				return results;
			}
			
			@Override
			public CharSequence convertResultToString(final Object resultValue) 
			{
				return resultValue == null ? "" : ((Address) resultValue).getAddressLine(0)+", "+((Address) resultValue).getAddressLine(1);
			}
		};
		return filter;
	}
}
