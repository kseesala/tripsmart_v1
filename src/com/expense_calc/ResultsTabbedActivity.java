package com.expense_calc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.data_classes.LocationData;
import com.data_classes.SavedData;
import com.data_classes.UserData;
import com.database.DatabaseSaveLoadActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class ResultsTabbedActivity extends FragmentActivity implements ActionBar.TabListener 
{

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;
	ArrayList<UserData> userdata;
	ArrayList<LocationData> locdata;
	String uri;
	ViewPager mViewPager;	
	Bundle extras;
	Geocoder gc;
	//SavedData sd;
	Boolean save;
	double mileage_1_gallon;
	double tank;
	
	boolean saved = false;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		//this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_expense_results);

		Gson gson = new Gson();
		gc = new Geocoder(ResultsTabbedActivity.this);
		
		extras = getIntent().getExtras();
		userdata = gson.fromJson(extras.getString("userdata"),new TypeToken<ArrayList<UserData>>(){}.getType());
		locdata = gson.fromJson(extras.getString("locdata"),new TypeToken<ArrayList<LocationData>>(){}.getType());
		uri = extras.getString("URI");
		save = extras.getBoolean("save");
		mileage_1_gallon = extras.getDouble("mileage_1_gallon");
		tank = extras.getDouble("tank");
		
		//Fill out the missing LatLng
		int i_temp = 0;
		for (LocationData element : locdata) 
		{
			if(element.loc.latitude == 0)
			{
				List<Address> temp_addr = null;
				try 
				{
					temp_addr = gc.getFromLocationName(element.name, 1);
				} 
				catch (IOException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.i("error at replacing address", e.toString());
				}
				locdata.get(i_temp).loc = new LatLng(temp_addr.get(0).getLatitude(), temp_addr.get(0).getLongitude());
			}
			i_temp++;
		}
		
		
		//sd = new SavedData(userdata, locdata, uri);
		
		
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) 
		{
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
	}


	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) 
	{
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) 
	{
		
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) 
	{
		
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter 
	{

		public SectionsPagerAdapter(FragmentManager fm) 
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int position) 
		{
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment;
			if(position == 0)
			{
				fragment = new UserSectionFragment();
			}
			else if (position == 1)
			{
				fragment = new LocationSectionFragment();
			}
			else
			{
				fragment = new ShowMapFragment();
			}
			return fragment;
		}

		@Override
		public int getCount() 
		{
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) 
		{
			switch (position) 
			{
			case 0:
				return "UserData";
			case 1:
				return "LocationData";
			case 2:
				return "ShowMap";
			}
			return null;
		}
	}

	public class UserSectionFragment extends Fragment
	{
		ListView lv;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
		{
			View view = inflater.inflate(R.layout.fragment_user_data, container, false);
			lv = (ListView)view.findViewById(R.id.list1);
			lv.setAdapter(new UserDataListAdapter(view.getContext(), R.layout.fragment_user_data, userdata));
			
			//Save the Data for use later on
			
			return view;
		}
	}
	
	public class LocationSectionFragment extends Fragment
	{
		ListView lv;
		TextView tv1,tv2;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
		{
			View view = inflater.inflate(R.layout.fragment_locdata, container, false);
			tv1 = (TextView)view.findViewById(R.id.mpg);
			tv2 = (TextView)view.findViewById(R.id.tank);
			lv = (ListView)view.findViewById(R.id.list1);
			tv1.setText("MPG: "+mileage_1_gallon);
			tv2.setText("Tank: "+tank);
			lv.setAdapter(new LocDataListAdapter(view.getContext(), R.layout.fragment_locdata, locdata));
			return view;
		}
	}
	
	public class ShowMapFragment extends Fragment
	{
		Button b1;
		Button b2;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
		{
			final View view = inflater.inflate(R.layout.fragment_show_map, container, false);
			b1 = (Button)view.findViewById(R.id.button1);
			b2 = (Button)view.findViewById(R.id.button2);
			
			if(save == false)
			{
				b2.setVisibility(View.GONE);
			}
			if(saved == true)
			{
				b2.setBackgroundResource(R.drawable.button_red);
			}
			
			b1.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
//					Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//					intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//					view.getContext().startActivity(intent);
					
					Gson gson = new Gson();					
					Intent intent = new Intent(ResultsTabbedActivity.this, MapResultExpenses.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("result", gson.toJson(locdata));
					intent.putExtra("result_user", gson.toJson(userdata));
					view.getContext().startActivity(intent);
					overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
				}
			});
			
			b2.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					if(saved == false)
					{
						SavedData sd = new SavedData(userdata, locdata, uri, mileage_1_gallon, tank);
						// TODO Auto-generated method stub
						Gson gson = new Gson();					
						Intent intent = new Intent(view.getContext(), DatabaseSaveLoadActivity.class);
						intent.putExtra("data", gson.toJson(sd));
						view.getContext().startActivity(intent);
						saved = true;
						b2.setBackgroundResource(R.drawable.button_red);
					}
					else
					{
						Toast.makeText(ResultsTabbedActivity.this, "You have already saved the data", Toast.LENGTH_LONG).show();
					}
				}
			});
			
			return view;
		}
	}
}
