package com.expense_calc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.data_classes.SavedData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.utility.StreamToString;

public class LoadDataAdapter extends ArrayAdapter<String> 
{
	Context context;
	ArrayList<String> data;
	String dir = "/";
	
	public LoadDataAdapter(Context context, int textViewResourceId,ArrayList<String> data, String dir) 
	{
		super(context, textViewResourceId, data);
		this.context = context;
		this.data = data;
		this.dir = dir;
		// TODO Auto-generated constructor stub
		
	}
	
	@Override
	public View getView(final int position, View convertView,final ViewGroup parent)
	{
		final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowview = inflater.inflate(R.layout.list_skeleton_saveddata, parent, false);
		TextView tv0 = (TextView)rowview.findViewById(R.id.textView1);
		Button b1 = (Button)rowview.findViewById(R.id.save);
		String temp = data.get(position); 
		tv0.setText(temp.substring(0,temp.length()-10));
		tv0.setVisibility(View.GONE);
		b1.setText(temp.substring(0,temp.length()-10));
		
		b1.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
//				Toast.makeText(context, "rowview clicked", Toast.LENGTH_LONG).show();
				
				Activity act = (Activity)context;
				
				final Dialog dialog = new Dialog(context);
				LayoutInflater inflater = act.getLayoutInflater();
				
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(inflater.inflate(R.layout.dialog_load_delete, null));
				
				Button b1,b2;
				b1 = (Button)dialog.findViewById(R.id.load_trip);
				b2 = (Button)dialog.findViewById(R.id.delete_trip);
				
				b1.setOnClickListener(new View.OnClickListener() 
				{
					
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						dialog.dismiss();
						try 
						{
							FileInputStream in = new FileInputStream(new File(dir+"/"+data.get(position)));
							StreamToString sts = new StreamToString();
							
							String str = sts.convertStreamtoString(in);
							
							Gson gson = new Gson();
							
							SavedData sd = gson.fromJson(str, new TypeToken<SavedData>(){}.getType()); 
							
							Intent intent = new Intent(context,ResultsTabbedActivity.class);
							intent.putExtra("userdata", gson.toJson(sd.userdata));
							intent.putExtra("locdata", gson.toJson(sd.locdata));
							intent.putExtra("mileage_1_gallon", sd.mpg);
							intent.putExtra("tank", sd.tank);
							intent.putExtra("URI",sd.URI);
							intent.putExtra("save", false);
							context.startActivity(intent);
						} 
						catch (FileNotFoundException e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				
				b2.setOnClickListener(new View.OnClickListener() 
				{
					
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						dialog.dismiss();
						File file = new File(dir+"/"+data.get(position));
						
						if(file.delete())
						{
							Toast.makeText(context, "File deleted successfully", Toast.LENGTH_LONG).show();
						}
						else
						{
							Toast.makeText(context, "Unable to delete file", Toast.LENGTH_LONG).show();
						}
						
						notifyDataSetChanged();
						Activity act = (Activity)context;
						Intent intent = act.getIntent();
						context.startActivity(intent);
						act.finish();
						act.overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_out);
						
						
					}
				});
				
				dialog.show();
			}
		});
		
		return rowview;
	}

}
