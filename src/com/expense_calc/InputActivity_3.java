package com.expense_calc;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.notice.Issue_Network;
import com.utility.Check_Internet;
import com.utility.PreViewOperations;

public class InputActivity_3 extends Activity 
{
	boolean check = false;
	String error_msg = "";
	Button b1;
	Geocoder geo;
	AutoCompleteTextView atv1,atv2;
	Check_Internet ic;
	InputMethodManager imm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		//For hiding the header
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		PreViewOperations pvo = new PreViewOperations(InputActivity_3.this, R.layout.activity_expense_calc_input_3, R.drawable.background_expense_calc);
		
		final View view = pvo.returnView();
		view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        //view.setOnFocusChangeListener(listener);

		super.onCreate(savedInstanceState);
		setContentView(view);
		
		ic = new Check_Internet(InputActivity_3.this);
		
		b1 = (Button)findViewById(R.id.button1);
		
		atv1 = (AutoCompleteTextView)findViewById(R.id.editText1);
		atv2 = (AutoCompleteTextView)findViewById(R.id.editText2);
		
		//Testing Data
		atv1.setText("Fairfax,VA");
		atv2.setText("5338 Sammie Kay Lane,Centreville,VA");
		//Testing Data
		
		geo = new Geocoder(getApplicationContext(), Locale.US);
		atv1.setAdapter(new AutocompleteAdapter(InputActivity_3.this, R.layout.activity_expense_calc_input_3));
		atv2.setAdapter(new AutocompleteAdapter(InputActivity_3.this, R.layout.activity_expense_calc_input_3));
		
		Bundle extras = getIntent().getExtras();
		final String no_of_people = extras.getString("no_of_people");
		final String mileage = extras.getString("mpg");
		final String tank = extras.getString("tank");
		final String fuel_type = extras.getString("fuel_type");
//		atv1.setText("5338 Sammie Kay Lane, Centreville");
//		atv2.setText("9411 Lee Hwy,Fairfax");
		
		atv1.setOnEditorActionListener(new OnEditorActionListener()
		{
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				// TODO Auto-generated method stub
				if(actionId==EditorInfo.IME_ACTION_NEXT)
				{
					getCurrentFocus().clearFocus();
		            atv2.requestFocus();
				}
				return false;
			}
		});
		
		atv2.setOnEditorActionListener(new OnEditorActionListener()
		{
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				// TODO Auto-generated method stub
				if(actionId==EditorInfo.IME_ACTION_DONE)
				{
					getCurrentFocus().clearFocus();
					imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(atv2.getWindowToken(), 0);
				}
				return false;
			}
		});
		
//		atv1.setOnKeyListener(new View.OnKeyListener()
//		{
//			@Override
//			public boolean onKey(View v, int keyCode, KeyEvent event)
//			{
//				// TODO Auto-generated method stub
//				if(keyCode == 66) 
//				{
//					
//		        }
//				return false;
//			}
//		});
		
		b1.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{				
				if(ic.isNetworkAvailable() == true)
				{
					//Address Verification Code
					try 
					{
						List<Address> temp = geo.getFromLocationName(atv1.getText().toString(), 3);
						if(temp.size()>0)
						{
							temp.clear();
							try
							{
								temp = geo.getFromLocationName(atv2.getText().toString(), 3);
							}
							catch(Exception e)
							{
								check = false;
								error_msg = e.toString(); 
							}
							if(temp.size()>0)
							{
								check = true;
							}
							else
							{
								error_msg = "Destination";
							}
						}
						else
						{
							error_msg = "Source";
						}
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
						error_msg = e.toString();
						check = false;
					}
					
					if(check == true)
					{
						String temp1 = atv1.getText().toString();
						String temp2 = atv2.getText().toString();
						if(temp1.equals(""))
						{
							atv1.setError("Missing Entry");
						}
						else
						{
							if(temp2.equals(""))
							{
								atv2.setError("Missing Entry");
							}
							else
							{
								Intent intent = new Intent(InputActivity_3.this, InputActivity_4.class);
								intent.putExtra("no_of_people",no_of_people);
								intent.putExtra("mileage", mileage);
								intent.putExtra("tank", tank);
								intent.putExtra("start", temp1);
								intent.putExtra("destination", temp2);
								intent.putExtra("fuel_type", fuel_type);
								startActivity(intent);
								finish();
								overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
							}
						}
					}
					else
					{
						if(error_msg.equals("Source"))
						{
//							Toast.makeText(InputActivity_3.this, "Please check your "+error_msg+" location and try again.", Toast.LENGTH_LONG).show();
							getCurrentFocus().clearFocus();
							atv1.requestFocus();
							atv1.setError("Please check your "+error_msg+" location and try again.");
						}
						else if(error_msg.equals("Destination"))
						{
							getCurrentFocus().clearFocus();
							atv2.requestFocus();
							atv2.setError("Please check your "+error_msg+" location and try again.");
						}
						else
						{
							Toast.makeText(InputActivity_3.this, error_msg, Toast.LENGTH_LONG).show();
						}
					}
				}
				else
				{
					Intent intent = new Intent(InputActivity_3.this, Issue_Network.class);
					startActivity(intent);
				}
			}
		});
	}
}
