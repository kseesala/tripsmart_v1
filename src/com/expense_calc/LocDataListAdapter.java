package com.expense_calc;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.application.tripsmart.R;
import com.data_classes.LocationData;
import com.data_classes.UserData;

public class LocDataListAdapter extends ArrayAdapter<LocationData> 
{
	Context context;
	ArrayList<LocationData> objects;
	
	public LocDataListAdapter(Context context,int textViewResourceId, ArrayList<LocationData> objects) 
	{
		super(context, textViewResourceId, objects);
		this.context = context;
		this.objects = objects;
	}
	
	@Override
	public View getView(final int position, View convertView,final ViewGroup parent)
	{
		View rowview = convertView;
		if(rowview == null)
		{
			ViewHolder viewholder = new ViewHolder();
			final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowview = inflater.inflate(R.layout.list_result_expenses_locdata, parent, false);
			viewholder.tv1 = (TextView)rowview.findViewById(R.id.textView1);
			viewholder.tv2 = (TextView)rowview.findViewById(R.id.textView2);
			viewholder.tv3 = (TextView)rowview.findViewById(R.id.textView3);
			viewholder.tv4 = (TextView)rowview.findViewById(R.id.textView4);
			viewholder.tv5 = (TextView)rowview.findViewById(R.id.textView5);
			viewholder.tv6 = (TextView)rowview.findViewById(R.id.textView6);
			viewholder.tv7 = (TextView)rowview.findViewById(R.id.textView7);
			viewholder.tv8 = (TextView)rowview.findViewById(R.id.textView8);
			viewholder.rowview = rowview;
			rowview.setTag(viewholder);
		}
				
		ViewHolder holder = (ViewHolder) rowview.getTag();
		DecimalFormat df = new DecimalFormat("##.###");
		LocationData temp = objects.get(position);
		holder.tv1.setText("Location: "+temp.name+" LatLng: "+temp.loc);
		holder.tv2.setText("Distance: "+temp.dist+"");
		holder.tv3.setText("Tag: "+temp.tag);
		holder.tv4.setText("Price: "+temp.price);
		holder.tv5.setText("Prev_Price: "+temp.prev_price);
		holder.tv6.setText("Crossed: "+temp.crossed.toString());
		String str = "[";
		for (UserData element : temp.user) 
		{
			str = str + element.name + " & ";
		}
		str = str.substring(0, str.length()-3);
		str = str + "]";
		holder.tv7.setText("User: "+str);
		holder.tv8.setText("Comment: "+temp.comment);
		return rowview;
	}
	
	static class ViewHolder
	{
		TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8;
		View rowview;
	}
}
