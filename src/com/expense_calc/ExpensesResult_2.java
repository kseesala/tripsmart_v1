package com.expense_calc;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.application.tripsmart.R;
import com.data_classes.GasDataClass;
import com.data_classes.LocationData;
import com.data_classes.UserData;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.utility.DistanceCalculator_Total_GMap;
import com.utility.PreViewOperations;

public class ExpensesResult_2 extends Activity 
{
	ArrayList<UserData> userdata;
	ArrayList<GasDataClass> gas_data;
	ArrayList<String> waypoints;
	String start = "";
	String destination = "";
	double mileage_1_gallon = 0;
	double tank = 0;
	ListView lv;
	
	DistanceCalculator_Total_GMap dctg;
	ArrayList<LocationData> ldc = new ArrayList<LocationData>();
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		PreViewOperations pvo = new PreViewOperations(ExpensesResult_2.this, R.layout.activity_expenses_result_2, R.drawable.background_expense_calc);
		
		super.onCreate(savedInstanceState);
		setContentView(pvo.returnView());
		
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_expenses_result_2);
		
		Bundle extras = getIntent().getExtras();
		Gson gson = new Gson();
		
		lv = (ListView)findViewById(R.id.list1);
		userdata = new ArrayList<UserData>();
		gas_data = new ArrayList<GasDataClass>();
		waypoints = new ArrayList<String>();
		
		userdata = gson.fromJson(extras.getString("userdata"), new TypeToken<ArrayList<UserData>>(){}.getType());
		gas_data = gson.fromJson(extras.getString("gas_stations"), new TypeToken<ArrayList<GasDataClass>>(){}.getType());
		waypoints = extras.getStringArrayList("waypoints");
		start = extras.getString("start");
		destination = extras.getString("destination");
		mileage_1_gallon = extras.getDouble("mileage_1_gallon");
		tank = extras.getDouble("tank");
		
		for (String element : waypoints) 
		{
			MultiThreadTask mtt = new MultiThreadTask();
			try 
			{
				//Latlng has been supplfied zero for now and will be processed later
				ldc.add(new LocationData(element, mtt.execute("",start,element).get(), "checkpoint",0, new LatLng(0, 0)));
			}
			catch (Exception e) 
			{
				Log.i("CheckpointException", e.toString());
			}
		}
		
		for (GasDataClass element : gas_data) 
		{
			MultiThreadTask mtt = new MultiThreadTask();
			try
			{
//				if(element.station.toLowerCase().equals("unbranded"))
//				{
//					element.station = "";
//				}
				String str = /*element.station+","+*/element.address+","+element.city+","+element.region+","+element.country;
				ldc.add(new LocationData(str, mtt.execute("",start,str).get(), "gas_station",element.price,element.location));
				ldc.get(ldc.size()-1).comment = element.station;
			}
			catch (Exception e) 
			{
				Log.i("GasPointException", e.toString());
			}
		}
		
		Log.i("gas_stations", gas_data.size()+"");
		Log.i("checkpoints", waypoints.size()+"");
		Log.i("loc_class", ldc.size()+"");
		
		Collections.sort(ldc);
		
		for (LocationData element : ldc) 
		{
			Log.i("name", element.name);
			Log.i("dist", element.dist+"");
			Log.i("tag", element.tag);
		}
		
		//Build URI for Google Maps
		String uri = "http://maps.google.com/maps?saddr=" + start+"&daddr=";
		int num = 0;
		for (LocationData element : ldc) 
		{
			String txt = "";
			if(num == 0)
			{
				txt = "";
			}
			else
			{
				txt = "+to:"; 
			}
			uri = uri + txt + "" + element.name;
			num++;
		}
		uri = uri + "+to:"+destination;
		Log.i("URI", uri);
				
		//ldc.get(0).tag = "gas_station";
		ldc.get(0).dist = 0;
		ldc.get(0).prev_price = ldc.get(0).price;
		//Latlng has been supplied zero for now and will be processed later
		ldc.add(0, new LocationData(start, 0, "start", 0, new LatLng(0, 0)));
		ldc.get(0).prev_price = 0;
		ldc.add(ldc.size(), new LocationData(destination, 0, "destination", 0, new LatLng(0, 0)));
		
		//Perfect Distances
		for(int i=0;i<ldc.size();i++)
		{
			ldc.get(i).user.addAll(userdata);
			LocationData tmp = ldc.get(i);
			if(i>0)
			{
				if(tmp.tag.equals("gas_station"))
				{
					for(int j=i-1;j>=0;j--)
					{
						LocationData tmp1 = ldc.get(j);
						String str = "";
						MultiThreadTask mtt1 = new MultiThreadTask();
						try
						{
							//ding
							if(j==i-1)
							{
								ldc.get(i).dist = mtt1.execute("",tmp1.name,tmp.name).get();
							}
//							while(ldc.get(i).dist!=0.0)
//							{
//								//ding
//								ldc.get(i).dist = mtt1.execute("",tmp1.name,tmp.name).get();
//								Thread.sleep(1000);
//							}
						}
						catch (Exception e) 
						{
							Log.i("perfect_distance", e.toString());
						}
						if(tmp1.tag.equals("gas_station")||tmp1.tag.equals("start"))
						{
							
							try 
							{
								ldc.get(i).crossed.add(tmp1.name);
//								ldc.get(i).dist = mtt1.execute(str,tmp1.name,tmp.name).get();
								ldc.get(i).prev_price = tmp1.price;
								Log.i("crossed check", ldc.get(i).crossed.toString());
								break;
							} 
							catch (Exception e) 
							{
								
							} 
						}
						else
						{
							str = str + tmp1.name+"|";
							ldc.get(i).crossed.add(tmp1.name);
						}
					}
				}
				else
				{
					for(int j=i-1;j>=0;j--)
					{
						LocationData tmp1 = ldc.get(j);
						String str = "";
						str = str + tmp1.name+"|";
						ldc.get(i).crossed.add(tmp1.name);
						if(j==(i-1))
						{
							ldc.get(i).price = tmp1.price;
							ldc.get(i).prev_price = tmp1.price;
						}
						if(j==i-1)
						{
							MultiThreadTask mtt1 = new MultiThreadTask();
							try 
							{
								//ding
								ldc.get(i).dist = mtt1.execute("",tmp1.name,tmp.name).get();
								if(ldc.get(i).tag.equals("start"))
								{
									
								}
								else
								{
//									while(ldc.get(i).dist!=0.0)
//									{
//										//ding
//										ldc.get(i).dist = mtt1.execute("",tmp1.name,tmp.name).get();
//										Thread.sleep(1000);
//									}
								}
							} 
							catch (Exception e) 
							{
								Log.i("perfect_distance1", e.toString());
							}
						}
					}
				}
			}
		}
		
		//Removal of Users
		int i = 0;
		for (UserData element : userdata) 
		{
			int j = 0;
			for (LocationData element1 : ldc) 
			{
				if(element.loc.equals(element1.name) && element1.tag.equals("checkpoint"))
				{
					for(int k=j+1;k<ldc.size();k++)
					{
						ldc.get(k).user.remove(element);
						ldc.get(k-1).comment = element.name+" gets off here";
					}
				}
				j++;
			}
			i++;
		}
		
		
		int x = 0;
		
		for (UserData element : userdata) 
		{
			userdata.get(x).pay = 0;
			for (LocationData element1 : ldc) 
			{
				int check = 0;
				for (UserData element2 : element1.user) 
				{
					if(element2.name.equals(element.name))
					{
						check  = 1;
						break;
					}
				}
				if(check == 1)
				{
					userdata.get(x).pay = userdata.get(x).pay + ((element1.dist*element1.prev_price)/(element1.user.size()*mileage_1_gallon));
					Log.i("Pay", userdata.get(x).name+" paid for "+element1.name+" with "+((element1.dist*element1.prev_price)/(element1.user.size()*mileage_1_gallon)));
				}
			}
			x++;
		}
		
		for (LocationData element : ldc) 
		{
			Log.i("name", element.name);
			Log.i("dist", element.dist+"");
			Log.i("tag", element.tag);
			Log.i("price", element.price+"");
			Log.i("prev_price", element.prev_price+"");
			Log.i("crossed", element.crossed.toString());
			for (UserData element1 : element.user) 
			{
				Log.i("users", element1.name);
			}		
		}
		
		for (UserData element : userdata) 
		{
			Log.i("user_pay", element.name+"="+element.pay);
		}
		
//		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
		//intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
		//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		startActivity(intent);
		
		Intent intent = new Intent(ExpensesResult_2.this, ResultsTabbedActivity.class);
		intent.putExtra("userdata", gson.toJson(userdata));
		intent.putExtra("locdata", gson.toJson(ldc));
		intent.putExtra("URI", uri);
		intent.putExtra("save", true);
		intent.putExtra("mileage_1_gallon", mileage_1_gallon);
		intent.putExtra("tank", tank);
		startActivity(intent);
		finish();
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		
//		Intent intent = new Intent(ResultActivity_Expenses.this,MapActivity_Expenses.class);
//		intent.putExtra("uri", uri);
//		startActivity(intent);
	}
	
	class MultiThreadTask extends AsyncTask<String, Void, Double>
	{
		@Override
		protected Double doInBackground(String... params)
		{
			double result = 0;
			dctg = new DistanceCalculator_Total_GMap(ExpensesResult_2.this, params[0]);				
			result = dctg.returnData(params[1],params[2]);
			return result;
		}
		
		@Override
		protected void onPostExecute(Double result)
		{
			
		}
	}
}
