package com.expense_calc;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.application.tripsmart.R;
import com.utility.PreViewOperations;

public class InputActivity_1 extends Activity 
{
	WheelView wv1,wv2;
	Button b1;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		//For hiding the header
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		PreViewOperations pvo = new PreViewOperations(InputActivity_1.this, R.layout.activity_expense_calc_input_1, R.drawable.background_expense_calc);
		
		super.onCreate(savedInstanceState);
		setContentView(pvo.returnView());
		
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_expense_calc_input_1);
		
		b1 = (Button)findViewById(R.id.button1);
		wv1 = (WheelView)findViewById(R.id.slot_2);
		//wv2 = (WheelView)findViewById(R.id.slot_3);
				
		wv1.setViewAdapter(new DateNumericAdapter(getApplicationContext(), 1, 19, 0));
		//wv2.setViewAdapter(new DateNumericAdapter(getApplicationContext(), 1, 9, 0));
		//wv2.setCurrentItem(0);
		
		
		//Image Resize Testing
		Bitmap bitimage = null;
		Options options = new Options();
		options.inJustDecodeBounds = true;
		RelativeLayout rl1 = (RelativeLayout)findViewById(R.id.rl1);
		//Image Resize Testing
		
		b1.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				String temp1 = ""+(wv1.getCurrentItem()+1)/*+""+(wv2.getCurrentItem()+1)*/;
				
				Intent intent = new Intent(InputActivity_1.this, InputActivity_2.class);
				intent.putExtra("no_of_people", temp1);
				startActivity(intent);
				finish();
				overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
			}
		});
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		super.onResume();
	}
	
	class DateNumericAdapter extends NumericWheelAdapter 
    {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;
        
        /**
         * Constructor
         */
        public DateNumericAdapter(Context context, int minValue, int maxValue, int current) {
            super(context, minValue, maxValue);
            this.currentValue = current;
            setTextSize(35);
        }
        
        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(Typeface.SANS_SERIF);
        }
        
        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }
}
