package com.expense_calc;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.application.tripsmart.R;
import com.utility.PreViewOperations;

public class InputActivity_2 extends Activity 
{
	WheelView wv1,wv2,wv3,wv4;
	Button b1;
	String[] type = {"reg","mid","pre"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		//For hiding the header
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		PreViewOperations pvo = new PreViewOperations(InputActivity_2.this, R.layout.activity_expense_calc_input_2, R.drawable.background_expense_calc);
		
		super.onCreate(savedInstanceState);
		setContentView(pvo.returnView());
		
		b1  = (Button)findViewById(R.id.button1);
		wv1 = (WheelView)findViewById(R.id.WheelView01);
		wv2 = (WheelView)findViewById(R.id.WheelView02);
		wv3 = (WheelView)findViewById(R.id.WheelView03);
		//wv4 = (WheelView)findViewById(R.id.WheelView04);
		
		wv1.setViewAdapter(new DateNumericAdapter(getApplicationContext(), 10, 50, 0));
		wv2.setViewAdapter(new DateNumericAdapter(getApplicationContext(), 10, 50, 0));
		wv3.setViewAdapter(new TextAdapter(InputActivity_2.this));
		//wv4.setViewAdapter(new DateNumericAdapter(getApplicationContext(), 0, 9, 0));
		
		wv1.setCurrentItem(0);
		wv2.setCurrentItem(0);
		wv3.setCurrentItem(0);
		//wv4.setCurrentItem(0);
		
		Bundle extras = getIntent().getExtras();
		final String no_of_people = extras.getString("no_of_people");
		
		b1.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				String temp1 = ""+(wv1.getCurrentItem()+10)/*+""+wv2.getCurrentItem()*/;
				String temp2 = ""+(wv2.getCurrentItem()+10)/*+""+wv4.getCurrentItem()*/;
				String temp3 = type[wv3.getCurrentItem()];
				
//				AlertDialog.Builder alert = new AlertDialog.Builder(InputActivity_2.this);
//				
//				//No Need for dialogs and checking as the min mileage has been increased and the min fuel tank capacity has been enhanced too
//				
//				final Dialog dialog = new Dialog(InputActivity_2.this);
//				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//				dialog.setContentView(R.layout.dialog_error1);
//				
//				TextView tv1 = (TextView)dialog.findViewById(R.id.heading);
//				TextView tv2 = (TextView)dialog.findViewById(R.id.content);
//				Button b1 = (Button)dialog.findViewById(R.id.ok);
//				
//				tv1.setText("Error");
//				
//				b1.setOnClickListener(new View.OnClickListener() 
//				{
//					
//					@Override
//					public void onClick(View v) 
//					{
//						// TODO Auto-generated method stub
//						if(dialog.isShowing()==true)
//						{
//							dialog.dismiss();
//						}
//					}
//				});
				
				if(!(temp1.equals("0")))
				{
					if(!(temp2.equals("0")))
					{
						Intent intent = new Intent(InputActivity_2.this, InputActivity_3.class);
						intent.putExtra("no_of_people", no_of_people);
						intent.putExtra("mpg", temp1);
						intent.putExtra("tank", temp2);
						intent.putExtra("fuel_type", temp3);
						startActivity(intent);
						finish();
						overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
					}
					else
					{
//						tv2.setText("Fuel tank capacity cannot be 0.");
//						dialog.show();
//						alert.setTitle("(Error)Fuel tank capacity cannot be 0");
//						alert.show();
					}
				}
				else
				{
//					tv2.setText("Mileage cannot be 0.");
//					dialog.show();
//					alert.setTitle("(Error)Mileage cannot be 0");
//					alert.show();
				}
			}
		});
	}
	
	class DateNumericAdapter extends NumericWheelAdapter 
    {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;
        
        /**
         * Constructor
         */
        public DateNumericAdapter(Context context, int minValue, int maxValue, int current) 
        {
            super(context, minValue, maxValue);
            this.currentValue = current;
            setTextSize(25);
        }
        
        @Override
        protected void configureTextView(TextView view) 
        {
            super.configureTextView(view);
            if (currentItem == currentValue) 
            {
                view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(Typeface.SANS_SERIF);
        }
        
        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) 
        {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }
	
	
	class TextAdapter extends AbstractWheelTextAdapter 
	{
        // Countries names
        private final String type[] =
            new String[] {"Regular","Mid-grade","Premium"};
        // Countries flags
        
        /**
         * Constructor
         */
        protected TextAdapter(Context context) 
        {
            super(context, R.layout.wheel_text, NO_RESOURCE);           
            setItemTextResource(R.id.text1);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) 
        {
            View view = super.getItem(index, cachedView, parent);
//            ImageView img = (ImageView) view.findViewById(R.id.flag);
//            img.setImageResource(flags[index]);
            return view;
        }
        
        @Override
        public int getItemsCount() 
        {
            return type.length;
        }
        
        @Override
        public CharSequence getItemText(int index) 
        {
            return type[index];
        }
    }
}
