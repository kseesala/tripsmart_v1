package com.expense_calc;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.application.tripsmart.R;
import com.data_classes.UserData;
import com.google.gson.Gson;
import com.utility.PreViewOperations;

//This activity represents the edit people screen, where the person name and their destination can be edited, The edited destination becomes the checkpoint but not the final destination
public class InputActivity_4 extends Activity 
{
	ArrayList<UserData> data;
	ListView lv;
	double mileage;
	Button b1;
	String start,loc;	
	double tank;
	String fuel_type;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		//For hiding the header
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		PreViewOperations pvo = new PreViewOperations(InputActivity_4.this, R.layout.activity_expense_calc_input_4, R.drawable.background_expense_calc);
		
		super.onCreate(savedInstanceState);
		setContentView(pvo.returnView());

		data = new ArrayList<UserData>();
		b1 = (Button)findViewById(R.id.button1);
		b1.setOnClickListener(new View.OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(InputActivity_4.this, ExpensesResult_1.class);
				String userdata_json = "[";
				Gson gson = new Gson();
				for (UserData element : data) 
				{
					userdata_json = userdata_json +gson.toJson(element) +",";
				}
				userdata_json = userdata_json + "]";
				intent.putExtra("user_json", userdata_json);
				intent.putExtra("start", start);
				intent.putExtra("destination", loc);
				intent.putExtra("mileage", mileage);
				intent.putExtra("tank",tank);
				intent.putExtra("fuel_type", fuel_type);
				startActivity(intent);
				finish();
				overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
			}
		});
		//getIntent.getExtras to get data from previous activity and store it in a bundle
		Bundle extras = getIntent().getExtras();
		Log.i("no of people", extras.getString("no_of_people"));
		int count = Integer.parseInt(extras.getString("no_of_people"));
		loc = extras.getString("destination");
		start = extras.getString("start");
		mileage = Double.parseDouble(extras.getString("mileage"));
		tank = Double.parseDouble(extras.getString("tank"));
		fuel_type = extras.getString("fuel_type");
		
		for(int i=0;i<count;i++)
		{
			//data.add(new UserData("person"+(i+1), loc, (i+1)));
			data.add(new UserData("John Doe", loc, (i+1)));
		}
		//We display all the items in terms of a listview using a custom adapter
		lv = (ListView)findViewById(android.R.id.list);
		lv.setAdapter(new InputActivity_4_Adapter(InputActivity_4.this, R.layout.activity_expense_calc_input_4, data,start));
	}
}
