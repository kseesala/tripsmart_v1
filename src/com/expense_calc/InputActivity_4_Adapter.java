package com.expense_calc;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.data_classes.UserData;

public class InputActivity_4_Adapter extends ArrayAdapter<UserData> 
{
	ArrayList<com.data_classes.UserData> objects;
	Context context;
	String start;
	public InputActivity_4_Adapter(Context context, int textViewResourceId, ArrayList<UserData> objects, String start) 
	{
		super(context, textViewResourceId,objects);
		this.objects = objects;
		this.context = context;
		this.start = start;		
	}
	
	@Override
	public View getView(final int position, View convertView,final ViewGroup parent)
	{
		View rowview = convertView;
		ViewHolder viewholder;
		if(rowview == null)
		{
			viewholder = new ViewHolder();
			final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowview = inflater.inflate(R.layout.list_skeleton_people_expenses, parent, false);
			viewholder.tv1 = (TextView)rowview.findViewById(R.id.name);
			viewholder.tv2 = (TextView)rowview.findViewById(R.id.destination);
			viewholder.tv3 = (TextView)rowview.findViewById(R.id.start);
			viewholder.tv4 = (TextView)rowview.findViewById(R.id.phno);
			viewholder.rowview = rowview;
			rowview.setTag(viewholder);
		}
		else
		{
			viewholder = (ViewHolder) rowview.getTag();
		}
			//Execute this code for every list item generate		
			
			//Testing data
//			if(position==2)
//			{
//				objects.get(position).loc = "Fair Oaks Mall, Sully, VA 22033, USA";
//			}
//			if(position==1)
//			{
//				objects.get(position).loc = "little rock ar";
//			}
			//Testing data
			
		ViewHolder holder = viewholder;
		UserData temp = objects.get(position);
		holder.tv1.setText(temp.name);
		
		holder.tv2.setText("Destination: "+temp.loc);
		
		holder.tv3.setText("Start: "+start);
		
		String phone_txt = objects.get(position).phno;
		phone_txt = "("+phone_txt.substring(0, 3)+") "+phone_txt.substring(3, 6)+"-"+phone_txt.substring(6, 10);
		
		holder.tv4.setText(phone_txt);
		
		holder.rowview.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{	
				
				Activity act = (Activity)context;
				Display dp = act.getWindowManager().getDefaultDisplay();
				
				final Dialog dialog = new Dialog(context);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_skeleton_destination);
				
				final AutoCompleteTextView atv1 = (AutoCompleteTextView)dialog.findViewById(R.id.atv1);
				atv1.setAdapter(new AutocompleteAdapter(context, R.layout.activity_expense_calc_input_4));
				final EditText ed1 = (EditText)dialog.findViewById(R.id.editText1);
				final EditText ed2 = (EditText)dialog.findViewById(R.id.editText2);

				Button b1 =(Button) dialog.findViewById(R.id.button1);
				Button b2 =(Button) dialog.findViewById(R.id.button2);
					
				ed1.setText(objects.get(position).name);	//7
				atv1.setText(objects.get(position).loc);	//13
				ed2.setText(objects.get(position).phno);	//4
				
				b1.setOnClickListener(new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				
				b2.setOnClickListener(new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						boolean check = true;
						String error_msg = "";
						Geocoder geo = new Geocoder(context);
						try
						{
							List<Address> temp = geo.getFromLocationName(atv1.getText().toString(), 3);
							if(temp.size()>0)
							{
								check = true;
							}
							else
							{
								check = false;
								error_msg = "Destination";
							}
							
							if(ed2.getText().toString().length()!=10)
							{
								check = false;
								error_msg = "Phoneno";
							}
						}
						catch(Exception e)
						{
							check = false;
							error_msg  = e.toString();
						}
						if(check == true)
						{
							objects.get(position).name = ed1.getText().toString();
							objects.get(position).loc = atv1.getText().toString();
							objects.get(position).phno = ed2.getText().toString();
							notifyDataSetChanged();
						}
						else
						{
							if(error_msg.equals("Destination"))
							{
								Toast.makeText(context, "Changes not saved please check your Destination and try again", Toast.LENGTH_LONG).show();
							}
							else if(error_msg.equals("Phoneno"))
							{
								Toast.makeText(context, "Please check the phone number", Toast.LENGTH_LONG).show();
							}
							else
							{
								Toast.makeText(context, error_msg, Toast.LENGTH_LONG).show();
							}
							
						}
						if(check==true && error_msg.equals(""))
						{
							Toast.makeText(context, "Data updated succesfully", Toast.LENGTH_LONG).show();
							dialog.dismiss();
						}
					}
				});
					
				dialog.show();
			}
		});
		return rowview;
	}
	
	static class ViewHolder
	{
		TextView tv1,tv2,tv3,tv4;
		View rowview;
	}
}

