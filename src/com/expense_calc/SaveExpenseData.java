package com.expense_calc;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.application.tripsmart.R;

public class SaveExpenseData extends Activity 
{

	Bundle extras; 
	public static final String DIRECTORY_NAME = "data";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_save_expense_data);
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();		
		extras = getIntent().getExtras();
		String str = extras.getString("data");
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
//		"dd|MM|yyyy|hhmmss"
		String filename = sdf.format(new Date())+".user_data";
		FileOutputStream out;
		//ProgressDialog_Custom pdc = new ProgressDialog_Custom(SaveExpenseData.this, "Writing Data to Device", width, height)
		
		try
		{
			out = openFileOutput(filename, Context.MODE_PRIVATE);
			out.write(str.getBytes());
			out.close();
		}
		catch (Exception e)
		{
			Log.i("filewriteexception",e.toString());
		}
		
		Toast.makeText(SaveExpenseData.this, "data saved to file:"+filename, Toast.LENGTH_LONG).show();
		finish();
	}

}
