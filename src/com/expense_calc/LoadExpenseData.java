package com.expense_calc;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ListView;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.utility.PreViewOperations;

public class LoadExpenseData extends Activity 
{

	ListView lv;
	ArrayList<String> file_names;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		PreViewOperations pvo = new PreViewOperations(LoadExpenseData.this, R.layout.activity_load_expense_data, R.drawable.load_background1);
		
		super.onCreate(savedInstanceState);
		setContentView(pvo.returnView());		
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		lv = (ListView) findViewById(R.id.list1);
		file_names = new ArrayList<String>();
		
		PackageManager m = getPackageManager();
		String s = getPackageName();
		try 
		{
		    PackageInfo p = m.getPackageInfo(s, 0);
		    s = p.applicationInfo.dataDir;
		} catch (NameNotFoundException e) 
		{
		    Log.i("yourtag", "Error Package name not found ", e);
		}
		
//		Toast.makeText(LoadExpenseData.this, "Current Dir is:"+s, Toast.LENGTH_LONG);
		
		s = s+"/files";
		File f = new File(s);
		File[] files = f.listFiles();
		
		if(files.length == 0)
		{
			Toast.makeText(LoadExpenseData.this, "No Files", Toast.LENGTH_LONG);
		}
		
		for (File element : files) 
		{
			if(element.getName().indexOf(".user_data")>1)
			{
				file_names.add(element.getName());
			}
			Collections.sort(file_names, Collections.reverseOrder());
		}
		
		lv.setAdapter(new LoadDataAdapter(LoadExpenseData.this, R.layout.activity_load_expense_data, file_names,s));		
		
	}

}
