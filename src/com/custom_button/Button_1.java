package com.custom_button;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class Button_1 extends Button 
{		
	public Button_1(Context context, AttributeSet attrs, int defStyle) 
	{
        super(context, attrs, defStyle);
        init();
    }

    public Button_1(Context context, AttributeSet attrs) 
    {
        super(context, attrs);
        init();
    }

    public Button_1(Context context) 
    {
        super(context);
        init();
    }

    private void init() 
    {
        if (!isInEditMode()) 
        {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/stryde_regular.otf");
            setTypeface(tf);
        }
    }
    
    @Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
	}
}
