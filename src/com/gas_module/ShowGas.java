package com.gas_module;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.custom_dialogs.ProgressDialog_1;
import com.custom_menu.CustomMenu;
import com.custom_menu.CustomMenu.OnMenuItemSelectedListener;
import com.custom_menu.CustomMenuItem;
import com.data_classes.GasDataClass;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.utility.Check_Internet;
import com.utility.GasUtility_v2;
import com.utility.PreViewOperations;

public class ShowGas extends Activity implements LocationListener,OnMenuItemSelectedListener,OnScrollListener
{
	LocationManager lm;
	boolean gps = false;
	boolean net = false;
	Location myloc = null;
	int no_of_tries = 0;
	
	Button b1;
	ProgressDialog_1 pd;
	
	Button current_label;
	private CustomMenu mMenu;
	public static final int MENU_ITEM_1 = 1;
	public static final int MENU_ITEM_2 = 2;
	public static final int MENU_ITEM_3 = 3;
	public static final int MENU_ITEM_4 = 4;	
	public String[] current = {
			"Fuel Type : Regular", 
			"Fuel Type : Midgrade", 
			"Fuel Type : Premium"
			};
		
	ArrayList<GasDataClass> data;
	double range = 10;	
//	boolean ok = false;
	ListView lv;
	GasArrayAdapter gaa;
	
	Check_Internet ic;
	
	AddSomeMoreItems addsome;
	
	ImageButton im1;
	
	TimerTask loc_timeout;
	Timer timer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		PreViewOperations pvo = new PreViewOperations(ShowGas.this, R.layout.activity_show_gas, R.drawable.background_find_gas);		
		
		super.onCreate(savedInstanceState);
		setContentView(pvo.returnView());
	}	
	
	@Override
	protected void onStart()
	{	
		super.onStart();
		
		lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		b1 = (Button)findViewById(R.id.label_current);
		im1 = (ImageButton)findViewById(R.id.map);
		lv =  (ListView)findViewById(android.R.id.list);
		data = new ArrayList<GasDataClass>();
		gaa = new GasArrayAdapter(ShowGas.this, data, myloc);
		lv.setAdapter(gaa);
		mMenu = new CustomMenu(this, this, getLayoutInflater());
        mMenu.setHideOnSelect(true);
        mMenu.setItemsPerLineInPortraitOrientation(4);
        mMenu.setItemsPerLineInLandscapeOrientation(8);
        //load the menu items
        loadMenuItems();
        
        ic = new Check_Internet(ShowGas.this);
		
		checkAndEnableListener();
		
		b1.setText(current[0]);
		
		
		im1.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{		
				Gson gson = new Gson();
				Intent intent = new Intent(ShowGas.this, ShowGasMap.class);
				intent.putExtra("data", gson.toJson(data));
				startActivity(intent);
			}
		});
	}

	@Override
	public void onLocationChanged(Location location) 
	{
		// TODO Auto-generated method stub
		if(myloc == null)
		{
			myloc = location;
		}
		else
		{
			long temp = location.getTime() - myloc.getTime();
			
			if(temp > (1000*10))
			{
				myloc = location;
			}
			else if(location.getAccuracy() > myloc.getAccuracy())
			{
				myloc = location;
			}
		}
		
		gaa.location = myloc;
		updateGas("reg");		
		lm.removeUpdates(this);
	}

	@Override
	public void onProviderDisabled(String provider) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) 
	{
		// TODO Auto-generated method stub
		
	}
			
	protected void checkAndEnableListener()
	{
		loc_timeout = new TimerTask()
		{
			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				if(no_of_tries < 10)
				{
					if(myloc==null)
					{
						no_of_tries++;
					}
				}
				else
				{
					if(pd.isShowing())
					{
						pd.dismiss();
					}
					timer.cancel();
					lm.removeUpdates(ShowGas.this);
					
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							// TODO Auto-generated method stub
							Toast.makeText(ShowGas.this, "Could not acquire your location", Toast.LENGTH_LONG).show();
						}
					});
					//Toast.makeText(ShowGas.this, "Could not acquire your location", Toast.LENGTH_SHORT).show();
					Log.i("error_loc", "Could not acquire location");
				}
			}
		};
		
		timer = new Timer();
		timer.schedule(loc_timeout, 100, 1000);
		
		if(pd == null)
		{
			Display dp = getWindowManager().getDefaultDisplay();
			pd = new ProgressDialog_1(ShowGas.this, "Working on it", dp.getWidth(), dp.getHeight());
			pd.show();
		}
		else if(!pd.isShowing())
		{
			pd.show();
		}
		
		if(lm.isProviderEnabled(LocationManager.GPS_PROVIDER))
		{
			gps = true;
		}
		
		if(lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
		{
			net = true;
		}
		
		if(gps == true)
		{
			lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		}
		if(net == true)
		{
			lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		}
	}
	
	protected void updateGas(Location location)
	{
		
	}	
	
	protected void updateGas(String fuel_type)
	{
		if(ic.isNetworkAvailable())
		{
//			ok = false;
			if(myloc == null)
			{
				no_of_tries++;
				checkAndEnableListener();
			}
			else
			{
				no_of_tries = 0;
				
				//Toast.makeText(ShowGas.this, "Location is"+myloc.toString(), Toast.LENGTH_LONG).show();				
				CalculateGas cg = new CalculateGas();
				try
				{
					//ok = cg.execute(fuel_type).get();
					cg.execute(fuel_type);
				}
				catch (Exception e)
				{
					Log.i("ShowGas",e.toString());
//					ok = false;
				}
			}
		}
		else
		{
			ic.gotoNetworkIssue();
		}
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	
	@Override
	protected void onResume()
	{
		super.onRestart();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{ 
	    if (keyCode == KeyEvent.KEYCODE_MENU) {
	    	doMenu();
	    	return true; //always eat it!
	    }
		return super.onKeyDown(keyCode, event); 
	} 
	
	private void loadMenuItems() 
	{
		//This is kind of a tedious way to load up the menu items.
		//Am sure there is room for improvement.
		ArrayList<CustomMenuItem> menuItems = new ArrayList<CustomMenuItem>();
		CustomMenuItem cmi = new CustomMenuItem();
		cmi.setCaption("Regular");
		cmi.setImageResourceId(R.drawable.icon_gas_pump);
		cmi.setId(MENU_ITEM_1);
		menuItems.add(cmi);
		cmi = new CustomMenuItem();
		cmi.setCaption("Midgrade");
		cmi.setImageResourceId(R.drawable.icon_gas_pump);
		cmi.setId(MENU_ITEM_2);
		menuItems.add(cmi);
		cmi = new CustomMenuItem();
		cmi.setCaption("Premium");
		cmi.setImageResourceId(R.drawable.icon_gas_pump);
		cmi.setId(MENU_ITEM_3);
		menuItems.add(cmi);
		cmi = new CustomMenuItem();
		cmi.setCaption("Sort By");
		cmi.setImageResourceId(R.drawable.icon_sort_descending);
		cmi.setId(MENU_ITEM_4);
		menuItems.add(cmi);
		if (!mMenu.isShowing())
		try 
		{
			mMenu.setMenuItems(menuItems);
		} 
		catch (Exception e) 
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Egads!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}

	private void doMenu() 
	{
		if (mMenu.isShowing()) 
		{
			mMenu.hide();
		} 
		else 
		{
			//Note it doesn't matter what widget you send the menu as long as it gets view.
			mMenu.show(findViewById(R.id.widget));
		}
	}
	
	@Override
	public void MenuItemSelectedEvent(CustomMenuItem selection) 
	{
		// TODO Auto-generated method stub
		
		String temp = Integer.toString(selection.getId()); 
		
		if(temp.equals("1"))
		{
			updateGas("reg");
			b1.setText(current[0]);
		}
		else if(temp.equals("2"))
		{
			updateGas("mid");
			b1.setText(current[1]);
		}
		else if(temp.equals("3"))
		{
			updateGas("pre");
			b1.setText(current[2]);
		}
		else if(temp.equals("4"))
		{
			if(pd.isShowing())
			{
				pd.dismiss();
			}
			
			if(data.size()!=0)
			{
				//Dialog to sort out the data
				final Dialog dialog = new Dialog(ShowGas.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_gas_sort_by);
				Display dp = getWindowManager().getDefaultDisplay();			
				Button ok = (Button)dialog.findViewById(R.id.ok);
				Button cancel = (Button)dialog.findViewById(R.id.cancel);
				final RadioGroup rg = (RadioGroup)dialog.findViewById(R.id.radio1);

				
				ok.setOnClickListener(new View.OnClickListener() 
				{			
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						if(rg.getCheckedRadioButtonId() == R.id.distance)
						{
							//Sort based on distance
							Collections.sort(data, new com.utility.Comparable("distance"));
							Toast.makeText(ShowGas.this, "List sorted based on distance", Toast.LENGTH_SHORT).show();
							gaa.notifyDataSetChanged();
						}
						else
						{
							//Sort based on price
							Collections.sort(data, new com.utility.Comparable("price"));
							Toast.makeText(ShowGas.this, "List sorted based on price", Toast.LENGTH_SHORT).show();
							gaa.notifyDataSetChanged();
						}
						dialog.dismiss();
					}
				});
				
				cancel.setOnClickListener(new View.OnClickListener() 
				{		
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				
				dialog.show();
			}
			else
			{
				Toast.makeText(ShowGas.this, "Cannot sort. Data not available", Toast.LENGTH_LONG).show();
			}
		}
	}
	
	class CalculateGas extends AsyncTask<String, Void, Boolean>
	{
				
		@Override
		protected void onPreExecute()
		{
			if(!pd.isShowing())
			{
				runOnUiThread(new Runnable() 
				{
					@Override
					public void run() 
					{
						// TODO Auto-generated method stub
						pd.show();
					}
				});
			}
		}

		@Override
		protected Boolean doInBackground(String... params) 
		{
			// TODO Auto-generated method stub
			try
			{
				GasUtility_v2 gu = new GasUtility_v2(ShowGas.this, new LatLng(myloc.getLatitude(), myloc.getLongitude()), range, params[0], "distance");
				data = gu.returnData();
				
				
			}
			catch(Exception e)
			{
				Log.i("fuckit", e.toString());
				return false;
			}
			
			if(data!=null && data.size()>0 )
			{
				//Data successfully obtained
				return true;
			}
			else
			{
				//Data not obtained
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(Boolean result)
		{
			if(result == true)
			{
//				gaa = new GasArrayAdapter(ShowGas.this, data, myloc);
				//lv.setAdapter(gaa);
				pd.dismiss();
				gaa.clear();
				gaa.addAll(data);
				gaa.notifyDataSetChanged();
				lv.setOnScrollListener(ShowGas.this);
				if(result)
				{
					//Location successfully updated
					Toast.makeText(ShowGas.this, "List updated successfully.", Toast.LENGTH_SHORT).show();
					Log.i("success", data.size()+"");
					//gaa.notifyDataSetChanged();
					
				}
				else
				{
					//Unable to update location
					Toast.makeText(ShowGas.this, "Updating unsuccessful.", Toast.LENGTH_LONG).show();
//					finish();
					Log.i("failure", "");
				}
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) 
	{
		// TODO Auto-generated method stub
		boolean loadmore = firstVisibleItem + visibleItemCount >= totalItemCount;
		if(loadmore && gaa.count < data.size())
		{
			addsome = new AddSomeMoreItems();
			addsome.execute(visibleItemCount);			
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) 
	{
		// TODO Auto-generated method stub
		
	}
	
	public class AddSomeMoreItems extends AsyncTask<Integer, Void, Void>
	{

		@Override
		protected Void doInBackground(Integer... params) 
		{
			try
			{
				gaa.count+= params[0];
				gaa.notifyDataSetChanged();
			}
			catch (Exception e) 
			{
				Log.i("error adding items", e.toString());
			}
			return null;
		}
		
		
		@Override
		public void onProgressUpdate(Void... progress)
		{
			//This part is not working
//			AlertDialog.Builder alert = new AlertDialog.Builder(ShowGas.this);
//			alert.setMessage("Adding some items");
//			alert.show();
		}
		
	}
	
}
