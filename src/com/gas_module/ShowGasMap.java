package com.gas_module;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.Window;

import com.application.tripsmart.R;
import com.custom_dialogs.ProgressDialog_1;
import com.data_classes.GasDataClass;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.ui.BubbleIconFactory;

public class ShowGasMap  extends FragmentActivity implements LocationListener 
{
	private GoogleMap map;
	private LocationManager locmanager;
	private String provider;
	private Polyline polyline;
	
	MarkerOptions markeroptions;
	
	ArrayList<GasDataClass> data;
	Bundle extras;
	
	LocationManager locm;
	boolean gps = false;
	boolean net = false;
	Location myloc = null;	
	ProgressDialog_1 pd;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_activity_show_gas);
		
		extras = getIntent().getExtras();
		locm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		map.setMyLocationEnabled(true);
		map.setIndoorEnabled(true);
		map.getUiSettings().setMyLocationButtonEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(true);
		map.getUiSettings().setZoomGesturesEnabled(true);
		map.getUiSettings().setAllGesturesEnabled(true);
		map.getUiSettings().setRotateGesturesEnabled(true);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		
		checkAndEnableListener();
		
	}
	
	protected void checkAndEnableListener()
	{
		
		if(pd == null)
		{
			Display dp = getWindowManager().getDefaultDisplay();
			pd = new ProgressDialog_1(ShowGasMap.this, "Working on it", dp.getWidth(), dp.getHeight());
			pd.show();
		}
		else if(!pd.isShowing())
		{
			pd.show();
		}
		
		if(locm.isProviderEnabled(LocationManager.GPS_PROVIDER))
		{
			gps = true;
		}

		if(locm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
		{
			net = true;
		}
		
		if(gps == true)
		{
			locm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		}
		if(net == true)
		{
			locm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		}
	}

	@Override
	public void onLocationChanged(Location location) 
	{
		// TODO Auto-generated method stub
		if(myloc == null)
		{
			myloc = location;
		}
		else
		{
			long temp = location.getTime() - myloc.getTime();
			
			if(temp > (1000*10))
			{
				myloc = location;
			}
			else if(location.getAccuracy() > myloc.getAccuracy())
			{
				myloc = location;
			}
		}
		
		doYourWork();
		locm.removeUpdates(this);
	}
	
	protected void doYourWork()
	{
		Gson gson = new Gson();
		ArrayList<GasDataClass> data = gson.fromJson(extras.getString("data"),new TypeToken<ArrayList<GasDataClass>>(){}.getType());
		
		for (GasDataClass element : data) 
		{
			BubbleIconFactory bubble = new BubbleIconFactory(ShowGasMap.this);
			bubble.setStyle(BubbleIconFactory.Style.DEFAULT);
			Bitmap icon = bubble.makeIcon(element.station+"\n"+element.distance+"\n"+element.price);
			MarkerOptions mk = new MarkerOptions().position(element.location);
//			mk.title(element.station);
//			mk.snippet("Distanct: "+element.distance+"\n Price: $"+element.price);
			mk.icon(BitmapDescriptorFactory.fromBitmap(icon));
			Marker marker_temp = map.addMarker(mk);
			String temp = element.distance.replace(" miles", "");
			if(Double.parseDouble(temp) > 5)
			{
				break;
			}
		}
		
		
		
		CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(myloc.getLatitude(),myloc.getLongitude()));
		CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
		map.moveCamera(center);
		map.animateCamera(zoom);
		
		map.setOnMarkerClickListener(new OnMarkerClickListener() 
		{	
			@Override
			public boolean onMarkerClick(Marker arg0) 
			{
				// TODO Auto-generated method stub
//				Toast.makeText(ShowGasMap.this, "Clic", Toast.LENGTH_SHORT).show();
			
				LatLng temp = arg0.getPosition();
				String uri = "http://maps.google.com/maps?saddr=" + myloc.getLatitude()+","+myloc.getLongitude()+"&daddr="+temp.latitude+","+temp.longitude;
				
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
				intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				
				finish();
				
				return false;
			}
		});
		
		pd.dismiss();
	}

	@Override
	public void onProviderDisabled(String provider) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) 
	{
		// TODO Auto-generated method stub
		
	}
}
