package com.gas_module;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.custom_dialogs.ProgressDialog_1;
import com.utility.StreamToString;

public class AsyncTask_PostData extends AsyncTask<String, Void, Boolean>
{
	
	String result;
	Context context;
	
	ProgressDialog_1 pd;
	
	public AsyncTask_PostData(Context context)
	{
		this.context = context;
	}
	
	@Override
	protected void onPreExecute()
	{
		pd = new ProgressDialog_1(context, "Working on it", 100, 100);
		pd.show();
	}

	@Override
	protected Boolean doInBackground(String... params) 
	{
		// TODO Auto-generated method stub
		
		HttpClient httpclient = new DefaultHttpClient();
		String uri = "http://tripsmart.us/_PHP/gasfeed.php?price="+params[0]+"&fueltype="+params[1]+"&stationid="+params[2];
		Log.i("URL", uri);
		HttpGet httpget = new HttpGet(uri);
		HttpResponse response;
		try
		{
			response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			if(entity!=null)
			{
				InputStream instream = entity.getContent();
				StreamToString st = new StreamToString();
				result = st.convertStreamtoString(instream);				
			}
		}
		//If unable to connect to network
		catch (UnknownHostException e) 
		{
//			context.startActivity(new Intent(context, NetworkIssue.class));
//			((Activity)context).finish();
		} catch (ClientProtocolException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONObject json;
		try 
		{
			json = new JSONObject(result);
			JSONObject json1 = json.getJSONObject("status");
			if(json1.getString("error").equals("NO"))
			{
				return true;
			}
			else
			{
				return false;
			}
		} 
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	@Override
	protected void onPostExecute(Boolean result)
	{
		pd.dismiss();
	}
	
}
