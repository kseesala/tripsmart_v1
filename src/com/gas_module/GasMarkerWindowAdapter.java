package com.gas_module;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.data_classes.GasDataClass;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class GasMarkerWindowAdapter implements InfoWindowAdapter 
{
	Context context;
	View contentsView;
	Activity act;
	
	public GasMarkerWindowAdapter(Context context, ArrayList<GasDataClass> data)
	{
		act = (Activity)context;
		contentsView = act.getLayoutInflater().inflate(R.layout.item_marker_show_gas, null);
	}

	@Override
	public View getInfoContents(Marker arg0) 
	{
		// TODO Auto-generated method stub
		TextView tv0,tv1,tv2;
		tv0 = (TextView) contentsView.findViewById(R.id.name);
		tv1 = (TextView) contentsView.findViewById(R.id.distance);
		tv2 = (TextView) contentsView.findViewById(R.id.price);
		
		contentsView.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				Toast.makeText(context, "You pressed something", Toast.LENGTH_LONG);
			}
		});
		
		return contentsView;
	}

	@Override
	public View getInfoWindow(Marker arg0) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}
