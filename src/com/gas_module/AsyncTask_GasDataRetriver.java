package com.gas_module;

import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;

import com.data_classes.GasDataClass;
import com.google.android.gms.maps.model.LatLng;
import com.utility.GasUtility_v2;

public class AsyncTask_GasDataRetriver extends AsyncTask<Void, Void, ArrayList<GasDataClass>> 
{
//	ProgressDialog_1 pd;
	Context context;
	LatLng location;
	double range = 0;
	String gas_type = "reg";
	boolean helper = false;
	String sort_by = "distance";
	
	GasDataClass gdc;
	String st = "";
	public String result;
	ArrayList<GasDataClass> data;
	
	public AsyncTask_GasDataRetriver(Context context, LatLng location, double range, String gas_type, String sort_by)
	{
		this.context = context;
		this.location = location;
		this.range = range;
		this.gas_type = gas_type;
		this.sort_by = sort_by;
	}
	
	@Override
	protected void onPreExecute()
	{
//		pd = new ProgressDialog_1(context, "Working on it.", 100, 100);
//		pd.show();
//		Dialog = new ProgressDialog(context);
//		Dialog.setMessage("Working on it.");
//		Dialog.show();
	}

	@Override
	protected ArrayList<GasDataClass> doInBackground(Void... params) 
	{
		GasUtility_v2 gu = new GasUtility_v2(context, location, range, gas_type, sort_by);
		data = gu.returnData();
		return data;
	}
	
	@Override
	protected void onPostExecute(ArrayList<GasDataClass> result)
    {
//		pd.dismiss();
//		Dialog.dismiss();
    }
}
