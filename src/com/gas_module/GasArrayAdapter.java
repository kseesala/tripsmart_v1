
package com.gas_module;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.custom_dialogs.UpdateGasDialog;
import com.data_classes.GasDataClass;
import com.google.android.gms.maps.model.LatLng;

public class GasArrayAdapter extends ArrayAdapter<GasDataClass>
{
	private final Context context;
	ArrayList<GasDataClass> objects;
	Location location;
	
	int count = 10;
	
	public GasArrayAdapter(Context context,ArrayList<GasDataClass> objects,Location location)
	{
		super(context, R.layout.activity_show_gas, objects);
		this.context = context;
		this.objects = objects;
		this.location = location;
	}
	
	@Override
	public View getView(final int position, View convertView,ViewGroup parent)
	{
		View rowview = convertView;
		
		if(rowview == null)
		{
			ViewHolder viewholder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowview = inflater.inflate(R.layout.list_skeleton_gasdata, parent, false);
			viewholder.tv0 = (TextView)rowview.findViewById(R.id.station_name);
			viewholder.tv1 = (TextView)rowview.findViewById(R.id.distance);
			viewholder.tv2 = (TextView)rowview.findViewById(R.id.price);
			viewholder.tv3 = (TextView)rowview.findViewById(R.id.date);
			viewholder.tv4 = (TextView)rowview.findViewById(R.id.click);
			viewholder.tv5 = (TextView)rowview.findViewById(R.id.address);
			viewholder.im0 = (ImageButton)rowview.findViewById(R.id.navigate);
			rowview.setTag(viewholder);
		}
		
		//rowview.setTag(tag);
		
		ViewHolder holder = (ViewHolder) rowview.getTag();
		
		
		//Setting Text to the Textviews
		String str = ("$"+objects.get(position).price);
		while(str.length()<5)
		{
			str = str + "0";
		}
		
		GasDataClass temp = objects.get(position);
		holder.tv0.setText(temp.station);
		holder.tv1.setText(""+temp.distance.replaceAll(" miles", ""));
		holder.tv2.setText(str);		
		holder.tv3.setText(temp.last_updated);
		holder.tv5.setText(temp.address+","+temp.city);
		
		//tv0.setTextColor(Color.BLACK);
		holder.tv1.setTextColor(Color.GRAY);
		holder.tv2.setTextColor(Color.RED);
		
		holder.tv3.setOnClickListener(new View.OnClickListener() 
		{
		
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				updateGasData(objects.get(position).gas_station_id);
			}
		});
		
		holder.tv4.setOnClickListener(new View.OnClickListener() 
		{
		
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				updateGasData(objects.get(position).gas_station_id);
			}
		});
		
		holder.im0.setOnClickListener(new View.OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage("Do you want to set navigation to this Gas Station?");
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{					
						//LocationUtility lu = new LocationUtility(context);
						//Check_GPS gc = new Check_GPS(context);
						//location = gc.returnLocation();
						LatLng tmp = objects.get(position).location;
						String uri = "http://maps.google.com/maps?saddr=" + location.getLatitude()+","+location.getLongitude()+"&daddr="+tmp.latitude+","+tmp.longitude;  
						Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
						intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.startActivity(intent);
					}
				});			
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{					
						Toast.makeText(context, "Ok. I won't do that.", Toast.LENGTH_SHORT).show();
					}
				});
				
				builder.show();
			}
			
		});
		
		return rowview;
	}
	
	@Override
	public int getCount()
	{
		if(count>objects.size())
		{
			return objects.size();
		}
		else
		{
			return count;
		}
	}
	
	public void updateGasData(String stationid)
	{
//		Toast.makeText(context, "testing", Toast.LENGTH_LONG).show();
		UpdateGasDialog ugd = new UpdateGasDialog(context, stationid);
		ugd.show();
		
	}
	
	static class ViewHolder
	{
		TextView tv0,tv1,tv2,tv3,tv4,tv5;
		ImageView im0;
	}
		
}
