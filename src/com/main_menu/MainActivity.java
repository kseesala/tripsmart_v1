package com.main_menu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.application.tripsmart.R;
import com.changelog.ChangeLog;
import com.custom_dialogs.ProgressDialog_1;
import com.database.DatabaseSaveLoadActivity;
import com.expense_calc.InputActivity_1;
import com.expense_calc.LoadExpenseData;
import com.gas_module.ShowGas;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.NoTitle;
import com.googlecode.androidannotations.annotations.ViewById;
import com.utility.PreViewOperations;

//Using Android Annotations

@NoTitle
@EActivity

//Code for MainMenu
public class MainActivity extends Activity 
{
	
	@ViewById (R.id.imageButton1)
	ImageButton im1;
	
	@ViewById (R.id.imageButton2)
	ImageButton im2;
	
	@ViewById (R.id.imageButton3)
	ImageButton im3;
	
	@ViewById (R.id.version)
	TextView tv;
	
	Intent intent;	
	ProgressDialog_1 pdc;
	ChangeLog cl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		//Hide the header 
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		PreViewOperations pvo = new PreViewOperations(MainActivity.this, R.layout.activity_main_menu, R.drawable.background_main_menu);
		View view = pvo.returnView();
		super.onCreate(savedInstanceState);
		setContentView(view);
		
		//Checks if First time and shows changelog
		cl = new ChangeLog(this);
		if (cl.firstRun())
	    {
			cl.getLogDialog().show();
	    }
		
//		ChangeLogDialog _ChangelogDialog = new ChangeLogDialog(this); 
//		_ChangelogDialog.show();  

//		im1 = (ImageButton)findViewById(R.id.imageButton1);
//		im2 = (ImageButton)findViewById(R.id.imageButton2);
//		im3 = (ImageButton)findViewById(R.id.imageButton3);		
//		tv = (TextView)findViewById(R.id.version);
		
		pdc = new ProgressDialog_1(MainActivity.this, "Working on it.", getWindowManager().getDefaultDisplay().getWidth(), getWindowManager().getDefaultDisplay().getHeight());
		
		tv.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				cl.getFullLogDialog().show();
			}
		});
		
		
		im1.setOnClickListener(new View.OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				//pd.show();
				
				final Dialog dialog = new Dialog(MainActivity.this);
				LayoutInflater inflater = getLayoutInflater();
				View view = inflater.inflate(R.layout.dialog_new_load, null);
				
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(view);
								
				Button b1,b2;
				b1 = (Button)dialog.findViewById(R.id.new_trip);
				b2 = (Button)dialog.findViewById(R.id.load_trip);
				
				b1.setOnClickListener(new View.OnClickListener() 
				{
					
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						dialog.dismiss();
						intent = new Intent(getApplicationContext(), InputActivity_1.class);
						startActivity(intent);
						overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
					}
				});
				
				b2.setOnClickListener(new View.OnClickListener() 
				{
					
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						dialog.dismiss();
						intent = new Intent(getApplicationContext(), LoadExpenseData.class);
						startActivity(intent);
						overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
					}
				});
				
				dialog.show();
			}
		});
		
		im2.setOnClickListener(new View.OnClickListener() 
		{	
			@Override
			public void onClick(View v)
			{
				intent = new Intent(getApplicationContext(), ShowGas.class);
				startActivity(intent);
				overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
			}
		});
		
		im3.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{				
				Intent intent = new Intent(MainActivity.this, DatabaseSaveLoadActivity.class);
				startActivity(intent);
//				ExitDialog dialog = new ExitDialog(MainActivity.this);
//				WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();  
//				lp.dimAmount=0.0f;  
//				dialog.getWindow().setAttributes(lp);  
//				dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);				
//				dialog.show();				
			}
		});
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		pdc.dismiss();
	}
}