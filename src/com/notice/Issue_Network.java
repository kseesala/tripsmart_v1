/*
 * Whenever a network issue is seen, this activity is brought to the front
 */

package com.notice;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.application.tripsmart.R;
import com.utility.PreViewOperations;

public class Issue_Network extends Activity 
{
	Button b1;
	public boolean finish = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
//		Display display = getWindowManager().getDefaultDisplay();
//		Point size = new Point();
//		display.getSize(size);
//		int width = size.x;
//		int height = size.y;
//		
//		Log.i("width", width+"");
//		Log.i("height", height+"");
		
		PreViewOperations pvo = new PreViewOperations(Issue_Network.this, R.layout.activity_issue_network, R.drawable.background_issue);
				
		super.onCreate(savedInstanceState);
		setContentView(pvo.returnView());
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		
		
		
		b1 = (Button)findViewById(R.id.button1);
		b1.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				//Intent intent = new Intent(NetworkIssue.this, MainActivity.class);
				//startActivity(intent);
				finish();
			}
		});
	}
}
