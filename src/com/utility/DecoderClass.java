/*
 * Used to decode from encoded text to latlng  positions
 */

package com.utility;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

//https://github.com/scoutant/polyline-decoder/blob/master/src/main/java/org/scoutant/polyline/PolylineDecoder.java

public class DecoderClass 
{	
	public ArrayList<LatLng> decodePoly(String encoded) 
	{
		ArrayList<LatLng> track = new ArrayList<LatLng>();
		int index = 0;
		int lat = 0, lng = 0;

		while (index < encoded.length()) 
		{
			int b, shift = 0, result = 0;
			do 
			{
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;

			shift = 0;
			result = 0;
			do 
			{
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;
			track.add(new LatLng( lat/1E5, lng/1E5 ));
		}
		return track;
	}
}
