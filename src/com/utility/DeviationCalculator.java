/*
 * Wrapper to use distance calculator total gmap
 */

package com.utility;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class DeviationCalculator// extends AsyncTask<LatLng, Void, Double>
{
	Context context;
	double deviation_temp = 0;
	public DeviationCalculator(Context context)
	{
		this.context = context;
	}
	
	public double returnData(LatLng... params)
	{
		double deviation = 0;
		LatLng gas_station = params[0];
		LatLng source = params[1];
		LatLng destination = params[2];
		DistanceCalculator_Total_GMap dctg = new DistanceCalculator_Total_GMap(context, gas_station.latitude+","+gas_station.longitude);
		try 
		{
			deviation = dctg.returnData(source.latitude+","+source.longitude,destination.latitude+","+destination.longitude);//dctg.execute(source.latitude+"/"+source.longitude+","+destination.latitude+"/"+destination.longitude).get(); 
		}
		catch (Exception e) 
		{
			Log.i("DeviationCalculator", e.toString());
		} 
		return deviation;
	}
	
//	protected Double doInBackground(LatLng... params) 
//	{
//		double deviation = 0;
//		LatLng gas_station = params[0];
//		LatLng source = params[1];
//		LatLng destination = params[2];
//		DistanceCalculator_Total_GMap dctg = new DistanceCalculator_Total_GMap(context, gas_station.latitude+","+gas_station.longitude);
//		try 
//		{
//			deviation = dctg.execute(source.latitude+"/"+source.longitude+","+destination.latitude+"/"+destination.longitude).get(); 
//		}
//		catch (Exception e) 
//		{
//			Log.i("DeviationCalculator", e.toString());
//		} 
//		return deviation;
//	}
}
