/*
 * Class to check availability of internet
 */

package com.utility;

import com.notice.Issue_Network;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

public class Check_Internet 
{
	Context context;
	
	public Check_Internet(Context context)
	{
		this.context = context;
	}
	
	public boolean isNetworkAvailable() 
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    if(activeNetworkInfo != null && activeNetworkInfo.isConnected())
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	public void gotoNetworkIssue()
	{
		Intent intent = new Intent(context,Issue_Network.class);
		context.startActivity(intent);
		Activity temp = (Activity)context;
		temp.finish();
	}
}
