/*
 * Calculates distance between two latlng points with waypoints using google maps
 */

package com.utility;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class DistanceCalculator_Total_GMap// extends AsyncTask<String, Void, Double> 
{
	double distance = 0;
	Context context;
	String waypoints = "";
	String str = "";
	String result = "";

	public DistanceCalculator_Total_GMap(Context context,String waypoints)
	{
		this.context = context;
		this.waypoints = waypoints;
	}
	
	public double returnData(String... params)
	{
		HttpClient httpclient = new DefaultHttpClient();
		if(waypoints.equals(""))
		{
			
		}
		else
		{
			waypoints = "&waypoints=optimize:true|"+waypoints.replace(" ", "%20");
		}
		str = "http://maps.googleapis.com/maps/api/directions/json?origin="+params[0].replace(" ", "%20")+"&destination="+params[1].replace(" ", "%20")+"&region=en&sensor=true"+waypoints;
		//Log.i("DistanceCalculator_Total_GMap", str);
		try 
		{
			str = str.replace("|", "%7C");
			URLEncoder.encode(str, "UTF-8");
		} 
		catch (UnsupportedEncodingException e1) 
		{
			Log.i("dafaq_again", e1.toString());
		}
		
		HttpGet httpget = new HttpGet(str);
		HttpResponse response;
		try
		{
			response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			if(entity!=null)
			{
				
				InputStream instream = entity.getContent();
				StreamToString st = new StreamToString();
				result = st.convertStreamtoString(instream);
				//Log.i("testing string dist", result);
				JSONObject jobject = new JSONObject(result);
				JSONArray routes = (JSONArray)jobject.get("routes");
				JSONArray legs = (JSONArray)((JSONObject)routes.get(0)).get("legs");
				
				for(int i=0;i<legs.length();i++)
				{
					JSONObject distance_list = (JSONObject)((JSONObject)legs.get(i)).get("distance");
					distance = distance + distance_list.getInt("value")*0.000621371;
				}
			}
		}
		catch (Exception e)
		{
			Log.i("DistanceCalculator_Total_GMap", e.toString()+"==="+str);
			return 0;
		}
		DecimalFormat df = new DecimalFormat("##.###");
		try 
		{
			Thread.sleep(500);
		} 
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Double.parseDouble(df.format(distance));
	}
	
//	protected Double doInBackground(String... params) 
//	{
//		HttpClient httpclient = new DefaultHttpClient();
//		String str = "http://maps.googleapis.com/maps/api/directions/json?origin="+params[0].replace(" ", "%20")+"&destination="+params[1].replace(" ", "%20")+"&region=en&sensor=true&waypoints=optimize:true|"+waypoints.replace(" ", "%20");
//		//Log.i("DistanceCalculator_Total_GMap", str);
//		try 
//		{
//			str = str.replace("|", "%7C");
//			URLEncoder.encode(str, "UTF-8");
//		} 
//		catch (UnsupportedEncodingException e1) 
//		{
//			Log.i("dafaq_again", e1.toString());
//		}
//		
//		HttpGet httpget = new HttpGet(str);
//		HttpResponse response;
//		try
//		{
//			response = httpclient.execute(httpget);
//			HttpEntity entity = response.getEntity();
//			if(entity!=null)
//			{
//				String result = "";
//				InputStream instream = entity.getContent();
//				StreamToString st = new StreamToString();
//				result = st.convertStreamtoString(instream);
//				JSONObject jobject = new JSONObject(result);
//				JSONArray routes = (JSONArray)jobject.get("routes");
//				JSONArray legs = (JSONArray)((JSONObject)routes.get(0)).get("legs");
//				
//				for(int i=0;i<legs.length();i++)
//				{
//					JSONObject distance_list = (JSONObject)((JSONObject)legs.get(i)).get("distance");
//					distance = distance + (double)(distance_list.getInt("value")*0.000621371);
//				}
//			}
//		}
//		catch (Exception e)
//		{
//			Log.i("DistanceCalculator_Total_GMap", e.toString());
//		}
//		DecimalFormat df = new DecimalFormat("##.###");
//		return Double.parseDouble(df.format(distance));
//	}
}
