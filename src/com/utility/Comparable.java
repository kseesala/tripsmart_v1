/*
 * Class to sort the GasDataClasses based on the input given
 */

package com.utility;

import java.util.Comparator;

import com.data_classes.GasDataClass;

public class Comparable implements Comparator<GasDataClass> 
{
	String type;
	public Comparable(String type)
	{
		this.type = type;
	}
	
	@Override
	public int compare(GasDataClass lhs, GasDataClass rhs) 
	{
		int res = 0;
		if(type.equals("distance"))
		{
			String str1 = lhs.distance; 
			String str2 = rhs.distance;
			str1 = str1.replaceAll("miles", "");
			str2 = str2.replaceAll("miles", "");
			double temp1 = Double.parseDouble(str1);
			double temp2 = Double.parseDouble(str2);
			if(temp1 > temp2)
			{
				res = 1;
			}
			else if(temp1 < temp2)
			{
				res = -1;
			}
		}
		else if(type.equals("deviation"))
		{
			double temp1 = lhs.deviation;
			double temp2 = rhs.deviation;
			if(temp1 > temp2)
			{
				res = 1;
			}
			else if(temp1 < temp2)
			{
				res = -1;
			}
		}
		else if(type.equals("price"))
		{
			//Sorting for price
			if(lhs.price > rhs.price)
			{
				res = 1;
			}
			else if(lhs.price < rhs.price)
			{
				res = -1;
			}
		}
		else if(type.equals("total_cost"))
		{
			double score1 = lhs.total_cost;
			double score2 = rhs.total_cost;
			if(score1 > score2)
			{
				res = 1;
			}
			else if(score1 < score2)
			{
				res = -1;
			}
		}
		return res;
	}
}
