package com.utility;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.application.tripsmart.R;

public class PreViewOperations 
{
	int drawableImage, drawableLayout;
	Context context;
	
	public PreViewOperations(Context context, int drawableLayout, int drawableImage)
	{
		this.drawableImage = drawableImage;
		this.drawableLayout = drawableLayout;
		this.context = context; 
	}
	
	public View returnView()
	{
		//Code to resize background image so there won't be a out of memory issue
		final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(drawableLayout, null, false);
		
		int width,height;
		
		width = 640;
		height = 480;
				
		double density = context.getResources().getDisplayMetrics().density;
		
		if(density == 0.75)
		{
			//LDPI
			width = context.getResources().getInteger(R.integer.ldpi_width);
			height = context.getResources().getInteger(R.integer.ldpi_height);
		}
		else if(density == 1.0)
		{
			//MDPI
			width = context.getResources().getInteger(R.integer.mdpi_width);
			height = context.getResources().getInteger(R.integer.mdpi_height);
		}
		else if (density == 1.5)
		{
			//HDPI
			width = context.getResources().getInteger(R.integer.hdpi_width);
			height = context.getResources().getInteger(R.integer.hdpi_height);
		}
		else if (density == 2.0)
		{
			//XHDPI
			width = context.getResources().getInteger(R.integer.xhdpi_width);
			height = context.getResources().getInteger(R.integer.xhdpi_height);
		}
		else if (density == 3.0)
		{
			//XXHDPI
			width = context.getResources().getInteger(R.integer.xxhdpi_width);
			height = context.getResources().getInteger(R.integer.xxhdpi_height);
		}
		
		Log.i("density is", density+"");
		
		BitmapDrawable bd = new BitmapDrawable(ImageResizer.decodeSampledBitmapFromResource(context.getResources(), drawableImage, width, height));
		
		Log.i("API version",android.os.Build.VERSION.SDK_INT+"");
		if(android.os.Build.VERSION.SDK_INT >= 16)
		{
			view.setBackground(bd);
		}
		else
		{
			view.setBackgroundDrawable(bd);
		}		
		
		return view;
	}
}
