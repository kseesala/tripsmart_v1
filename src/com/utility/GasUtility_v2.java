/*
 * Class to retrieve gas data from the network
 */

package com.utility;

import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.data_classes.GasDataClass;
import com.google.android.gms.maps.model.LatLng;

public class GasUtility_v2
{	
	LatLng location;
	double range = 0;
	String gas_type = "reg";
	String sort_by = "distance";
	
	GasDataClass gdc;
	String st = "";
	public String result;
	ArrayList<GasDataClass> data;
	
	public GasUtility_v2(Context context, LatLng location, double range, String gas_type, String sort_by)
	{
		
		this.location = location;
		this.range = range;
		this.gas_type = gas_type;
		this.sort_by = sort_by;
	}
	
	
	public ArrayList<GasDataClass> returnData()
	{
		HttpClient httpclient = new DefaultHttpClient();
		String uri = "http://api.mygasfeed.com/stations/radius/"+location.latitude+"/"+location.longitude+"/"+range+"/"+gas_type+"/"+sort_by+"/hqw3beot9h.json?callback=?";
		Log.i("URI",uri);
		HttpGet httpget = new HttpGet(uri);

		HttpResponse response;
		try
		{
			response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			if(entity!=null)
			{
				InputStream instream = entity.getContent();
				StreamToString st = new StreamToString();
				result = st.convertStreamtoString(instream);				
			}
		}
		//If unable to connect to network
		catch (UnknownHostException e) 
		{
//			context.startActivity(new Intent(context, NetworkIssue.class));
//			((Activity)context).finish();
		}
		//Some other issue
		catch(Exception e)
		{
			Log.i("myexception", e.toString());
//			context.startActivity(new Intent(context, Issue.class));
//			((Activity)context).finish();
		}
		
		//Result found, therefore return it in Arraylist of objects
		if(result != null)
		{
			//Remove useless brackets from the result
			result = result.substring(2, result.length()-2);
					
			//Parse the string result to make objects
			data = new ArrayList<GasDataClass>();
			try
			{
				//Log.i("testingtag","The Result is:"+result);
				JSONObject json = new JSONObject(result);
				JSONArray arr = (JSONArray)json.get("stations");
				JSONObject temp;
				
				//final for full json, distance for least distance(only one), price for low price(only one)
				String temp1 = "final";	
				for(int i=0;i<arr.length();i++)
				{
					temp = (JSONObject)arr.get(i);
					temp1 = temp1 + temp.getString("station");
					if(temp.getString("price").equals("N/A"))
					{
						//Do nothing
					}
					else
					{
						data.add(new GasDataClass(temp.getDouble("price"), temp.getString("date"), temp.getString("address"), temp.getString("city"), temp.getString("region"), temp.getString("country"), temp.getString("zip"), temp.getInt("diesel"), temp.getDouble("lat"), temp.getDouble("lng"), temp.getString("station"), temp.getString("distance"), temp.getString("date"), temp.getString("id")));
					}
				}			
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				Log.i("errortag", e.toString());
			}
			
			//Return the final result
			Log.i("data", data.size()+"");
			return data;
		}
		//No result thereby return null
		else
		{
			return null;
		}
	}
}
