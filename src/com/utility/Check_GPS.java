/*
 * Class to Check for GPS and also return location based on GPS
 */

package com.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;


public class Check_GPS 
{
	Context context;
	LocationManager locmanager;
	public boolean finish = true;
	LocationListener ll;
	Location myloc;
	
	
	public Check_GPS(Context context)
	{
		this.context = context;
		this.locmanager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
	}
	
	public boolean isGPSAvailable() 
	{			
		return locmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}
	
	public Location returnLocation()
	{
		return locmanager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}
	
	public void gotoGPSSettings()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage("Your GPS doesn't seem to be switched on, Click Yes to proceed to GPS settings or No to continue without GPS.");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				context.startActivity(intent);
				//Activity temp = (Activity)context;
				if(finish == true)
				{
					//temp.finish();		
				}
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				//Activity temp  = (Activity)context;
				//temp.finish();
			}
		});
		builder.show();
	}
}
