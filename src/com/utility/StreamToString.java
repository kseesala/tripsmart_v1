/*
 * Class that converts stream to string
 */

package com.utility;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StreamToString 
{
	public String convertStreamtoString(InputStream is)
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try
		{
			while((line = br.readLine())!=null)
			{
				sb.append(line+"\n");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				is.close();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return sb.toString();
		
	}
}
