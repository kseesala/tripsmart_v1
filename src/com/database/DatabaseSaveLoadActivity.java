package com.database;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.data_classes.SavedData;
import com.expense_calc.ResultsTabbedActivity;
import com.google.gson.Gson;

public class DatabaseSaveLoadActivity extends Activity
{
	DatabaseOperations dbo;
	SavedData sd;
	ListView lv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_database_save);
	} 
	
	@Override
	protected void onStart()
	{
		super.onStart();
		
		lv = (ListView)findViewById(R.id.list1);
		
		sd = null;
		
//		Bundle extras = getIntent().getExtras();
//		String str = extras.getString("data");
//		
//		Gson gson = new Gson();
//		SavedData sd = gson.fromJson(str, new TypeToken<SavedData>(){}.getType());
		
		dbo = new DatabaseOperations(DatabaseSaveLoadActivity.this, sd);
		
		ArrayList<String> data = new ArrayList<String>();
		try
		{
//			dbo.saveData();
			data = dbo.getAllTrips();
//			dbo.readFromDatabase_Trip_User();
//			dbo.readFromDatabase_User_Data();
//			dbo.readFromDatabase_Location_Data();
			sd = dbo.loadData("081513175110");

			Gson gson = new Gson();
			Intent intent = new Intent(DatabaseSaveLoadActivity.this,ResultsTabbedActivity.class);
			intent.putExtra("userdata", gson.toJson(sd.userdata));
			intent.putExtra("locdata", gson.toJson(sd.locdata));
			intent.putExtra("mileage_1_gallon", sd.mpg);
			intent.putExtra("tank", sd.tank);
			intent.putExtra("URI",sd.URI);
			intent.putExtra("save", false);
//			startActivity(intent);
//			finish();
			
			Toast.makeText(DatabaseSaveLoadActivity.this, sd.userdata.size()+" Yeah the data"+sd.locdata.size(), Toast.LENGTH_LONG).show();
		}
		catch (Exception e)
		{
			Log.i("addtrip",e.toString());
		}
//		Toast.makeText(DatabaseSaveActivity.this, data.size()+"", Toast.LENGTH_LONG).show();
		Collections.sort(data);
		lv.setAdapter(new DataBaseLoadDataAdapter(DatabaseSaveLoadActivity.this, R.layout.activity_database_save, dbo, data));
//		lv.setAdapter(new ArrayAdapter<String>(DatabaseSaveLoadActivity.this, android.R.layout.simple_list_item_1, data));
	}
}
