package com.database;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DatabaseHandler extends SQLiteOpenHelper 
{
	Context context;

	//Database Version
	public static final int DATABASE_VERSION = 2;
	
	//Database Name
	public static final String DATABASE_NAME = "Tripsmart";
	
	//Table Names	
	public static final String[] TABLE_NAMES = {
		"trip",
		"user_data",
		"location_data",
		"trip_user",
		"trip_loc_user"
	};
	
	//Table Columns 
	public static final String[][] TABLE_COLUMNS = {
		{"trip_id","mpg","tank"},
		{"user_id","user_name","user_no"},
		{"trip_id","loc_id","loc_name","dist","tag","price","prev_price","latitude","longitude","loc_comment"},
		{"trip_id","user_id","distance","pay","loc"},
		{"trip_id","loc_id","user_id"}
	};
	
	public static final String[] TABLE_CREATE = {
		"create table trip(trip_id varchar2(15), mpg number, tank number, uri varchar(100), primary key (trip_id));",
		"create table user_data(user_id number, user_name varchar2(30), user_no varchar2(15), primary key (user_id));",
		"create table location_data(trip_id varchar2(15), loc_id number, loc_name varchar2(50), dist float, tag varchar2(15), price float, prev_price float, latitude float, longitude float, loc_comment varchar2(50), foreign key (trip_id) references trip(trip_id) on delete cascade, primary key (trip_id,loc_id));",
		"create table trip_user(trip_id varchar2(15), user_id number, distance float, pay float, loc varchar2(40), foreign key (trip_id) references trip(trip_id) on delete cascade, foreign key (user_id) references user_data(user_id) on delete cascade);",
		"create table trip_loc_user(trip_id varchar2(15), loc_id number, user_id number, foreign key (trip_id,loc_id) references location_data(trip_id,loc_id) on delete cascade, foreign key (user_id) references user_data(user_id) on delete cascade);"
	};
	
	//Database Copy Path
	public static final String DATABASE_PATH = "/data/data/com.application.tripsmart/databases/";
	
	public DatabaseHandler(Context context) 
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		// TODO Auto-generated method stub
		
		for (String element : TABLE_CREATE) 
		{
			db.execSQL(element);
		}
		
		copyDatabase();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		// TODO Auto-generated method stub
		for (String element : TABLE_NAMES) 
		{
			db.execSQL("DROP TABLE IF EXISTS " + element);
		}
		onCreate(db);
		copyDatabase();
	}
	
	private void copyDatabase()
	{
		try
		{
		
			InputStream myinput = context.getAssets().open(DATABASE_NAME);
			String outFilename = DATABASE_PATH + DATABASE_NAME;
			Log.i("output", outFilename);
			OutputStream myoutput = new FileOutputStream(outFilename);
					
			//transfer bytes from the inputfile to the outputfile
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myinput.read(buffer))>0)
			{
				myoutput.write(buffer, 0, length);
			}
			
			Toast.makeText(context, "Database Copied Succesfully", Toast.LENGTH_LONG).show();
			
			//Close the streams
			myoutput.flush();
			myoutput.close();
			myinput.close();
		}
		catch (Exception e)
		{
			Log.i("Copy Error", e.toString());
		}
	}
}
