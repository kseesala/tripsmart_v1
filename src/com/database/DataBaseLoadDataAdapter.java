package com.database;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.data_classes.SavedData;
import com.expense_calc.ResultsTabbedActivity;
import com.google.gson.Gson;

public class DataBaseLoadDataAdapter extends ArrayAdapter<String>
{
	Context context;
	ArrayList<String> data = new ArrayList<String>();
	DatabaseOperations dbo; 
	
	public DataBaseLoadDataAdapter(Context context, int textViewResourceId, DatabaseOperations dbo, ArrayList<String> data)
	{
		super(context, textViewResourceId, data);
		this.context = context;
		this.data = data;
		this.dbo = dbo;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(final int position, View convertView,final ViewGroup parent)
	{
		final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowview = inflater.inflate(R.layout.list_skeleton_saveddata, parent, false);
		TextView tv0 = (TextView)rowview.findViewById(R.id.textView1);
		Button b1 = (Button)rowview.findViewById(R.id.save);
		final String temp = data.get(position); 
//		tv0.setText(temp.substring(0,temp.length()-10));
		tv0.setVisibility(View.GONE);
		b1.setText(temp);
		
		b1.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				ArrayList<String> data = new ArrayList<String>();
				try
				{
//					dbo.saveData();
					data = dbo.getAllTrips();
//					dbo.readFromDatabase_Trip_User();
//					dbo.readFromDatabase_User_Data();
//					dbo.readFromDatabase_Location_Data();
//					SavedData sd = dbo.loadData("081513175110");
					SavedData sd = dbo.loadData(temp);

					Gson gson = new Gson();
					Intent intent = new Intent(context,ResultsTabbedActivity.class);
					intent.putExtra("userdata", gson.toJson(sd.userdata));
					intent.putExtra("locdata", gson.toJson(sd.locdata));
					intent.putExtra("mileage_1_gallon", sd.mpg);
					intent.putExtra("tank", sd.tank);
					intent.putExtra("URI",sd.URI);
					intent.putExtra("save", false);
					context.startActivity(intent);
//					finish();
					
					Toast.makeText(context, sd.userdata.size()+" Yeah the data"+sd.locdata.size(), Toast.LENGTH_LONG).show();
				}
				catch (Exception e)
				{
					Log.i("addtrip",e.toString());
				}
			}
		});
		
		
		return rowview;
	}
	
}
