package com.database;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.data_classes.LocationData;
import com.data_classes.SavedData;
import com.data_classes.UserData;
import com.google.android.gms.maps.model.LatLng;


public class DatabaseOperations
{
	DatabaseHandler dbh;
	SQLiteDatabase db;
	boolean user_exists;
	SavedData sd;
	Context context;
	
	public DatabaseOperations(Context context, SavedData sd)
	{
		dbh = new DatabaseHandler(context);
		this.context = context;
		this.sd = sd;
	}
	
	public void openDatabase()
	{
		db = dbh.getWritableDatabase();
	}
	
	public void closeDatabase()
	{
		dbh.close();
	}
	
	public void saveData()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("MMddyyHHmmss");
		//String filename = sdf.format(new Date())+".user_data";
		String trip_id = sdf.format(new Date());
		try
		{
			android.util.Log.i("trip_id",trip_id);
//			String trip_id_int = Integer.parseInt(trip_id);
			
			//Add Trip
			addTrip(trip_id, (int)sd.mpg, (int)sd.tank);
//			addTrip(trip_id, 10, 10);
			
			//Add User Data
			for (UserData element : sd.userdata)
			{
				addUserData(trip_id, element);
			}
			
			int i = 0;
			//Add Location Data		
			for (LocationData element : sd.locdata)
			{
				Log.i("locdata", element.tag);
				addLocationData(trip_id, i, element.name, element.dist, element.tag, element.price, element.prev_price, element.loc.latitude, element.loc.longitude, element.comment);
				
				//Add User with Location
				for (UserData element1 : element.user)
				{
					Log.i("userdata", element1.user_id+"");
					addUserWithLocation(trip_id, i, element1.user_id);					
				}
				i++;
			}
		}
		catch (Exception e)
		{
			Log.i("Some stupid Error", e.toString());
		}
	}
	
	public SavedData loadData(String trip_id)
	{
		SavedData sd= new SavedData(getUsersForATripOrLoc(trip_id, 1, false), getAllLocDataForATrip(trip_id), "", 10, 10);
		return sd;
	}
	
	public void addTrip(String trip_id, int mpg, int tank)
	{
		openDatabase();
		ContentValues cv = new ContentValues();
		
		//Trip
		cv.put(DatabaseHandler.TABLE_COLUMNS[0][0], trip_id);
		cv.put(DatabaseHandler.TABLE_COLUMNS[0][1], mpg);
		cv.put(DatabaseHandler.TABLE_COLUMNS[0][2], tank);
		db.insert(DatabaseHandler.TABLE_NAMES[0], null, cv);
		Toast.makeText(context, "0) Successfully Saved trip", Toast.LENGTH_SHORT).show();
	}
		
	private boolean checkforExistingUserID(int user_id)
	{
		// TODO Auto-generated method stub
		String listQuery = "SELECT  user_id FROM " + DatabaseHandler.TABLE_NAMES[1];		
        openDatabase();
        
        ArrayList<Integer> result = new ArrayList<Integer>();
        Cursor cursor = db.rawQuery(listQuery, null);
        if(cursor.moveToFirst())
        {
        	do
        	{
        		result.add(cursor.getInt(0));
        	}
        	while(cursor.moveToNext());
        }
        
        if(result.size()>0)
		{
        	return true;
		}
        else
        {
        	return false;
        }
        	
	}
	
	public void addUserData(String trip_id, UserData data)
	{
		openDatabase();
		ContentValues cv1 = new ContentValues();
		ContentValues cv2 = new ContentValues();
		ContentValues cv3 = new ContentValues();
		
		boolean user_exists = checkforExistingUserID(data.user_id);
		
		//User_Data
		if(!user_exists)
		{
			//Adds new user entries to table
			cv1.put(DatabaseHandler.TABLE_COLUMNS[1][0], data.user_id);
			cv1.put(DatabaseHandler.TABLE_COLUMNS[1][1], data.name);
			cv1.put(DatabaseHandler.TABLE_COLUMNS[1][2], data.phno);
			db.insert(DatabaseHandler.TABLE_NAMES[1], null, cv1);
			Toast.makeText(context, "1) Successfully Saved user_data", Toast.LENGTH_SHORT).show();
		}
		
		//Trip_User
		//Adds user entries to trip
		cv2.put(DatabaseHandler.TABLE_COLUMNS[3][0], trip_id);
		cv2.put(DatabaseHandler.TABLE_COLUMNS[3][1], data.user_id);
		cv2.put(DatabaseHandler.TABLE_COLUMNS[3][2], data.distance);
		cv2.put(DatabaseHandler.TABLE_COLUMNS[3][3], data.pay);
		cv2.put(DatabaseHandler.TABLE_COLUMNS[3][4], data.loc);
		db.insert(DatabaseHandler.TABLE_NAMES[3], null, cv2);
		Toast.makeText(context, "2) Successfully Saved trip_user "+data.name, Toast.LENGTH_SHORT).show();
	}

	public void addLocationData(String trip_id, int loc_id, String loc_name, double dist, String tag, double price, double prev_price, double latitude, double longitude, String loc_comment)
	{
		openDatabase();
		ContentValues cv1 = new ContentValues();
		
		//Location_Data
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][0], trip_id);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][1], loc_id);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][2], loc_name);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][3], dist);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][4], tag);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][5], price);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][6], prev_price);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][7], latitude);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][8], longitude);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[2][9], loc_comment);
		db.insert(DatabaseHandler.TABLE_NAMES[2], null, cv1);
		Toast.makeText(context, "3) Successfully Saved location_data "+loc_name, Toast.LENGTH_SHORT).show();
	}
	
	public void addUserWithLocation(String trip_id, int loc_id, int user_id)
	{
		openDatabase();
		ContentValues cv1 = new ContentValues();
		
		//Trip_Loc_User
		cv1.put(DatabaseHandler.TABLE_COLUMNS[4][0], trip_id);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[4][1], loc_id);
		cv1.put(DatabaseHandler.TABLE_COLUMNS[4][2], user_id);
		db.insert(DatabaseHandler.TABLE_NAMES[4], null, cv1);
		Toast.makeText(context, "4) Successfully Saved trip_loc_user", Toast.LENGTH_SHORT).show();
	}
	
	public ArrayList<String> getAllTrips()
	{		
		String listQuery = "SELECT  trip_id FROM " + DatabaseHandler.TABLE_NAMES[0];		
        openDatabase();
        ArrayList<String> data = new ArrayList<String>();
        Cursor cursor = db.rawQuery(listQuery, null);
        if(cursor.moveToFirst())
        {
        	do
        	{
        		String str = cursor.getString(0);
        		data.add(str);
        	}
        	while(cursor.moveToNext());
        }
        return data;
        
	}
	
	public ArrayList<UserData> getUsersForATripOrLoc(String trip_id, int loc_id, boolean loc_id_exists)
	{
		String listQuery;
		if(!loc_id_exists)
		{
			listQuery = "select t1.user_id,t1.user_name,t1.user_no, t2.distance, t2.pay, t2.loc from "+DatabaseHandler.TABLE_NAMES[1]+" t1,"+DatabaseHandler.TABLE_NAMES[3]+" t2 where t1.user_id=t2.user_id and t2.trip_id like '"+trip_id+"'";
			Log.i("query",listQuery);
		}
		else
		{
			listQuery = "select t1.user_id,t1.user_name,t1.user_no, t2.distance, t2.pay, t2.loc from "+DatabaseHandler.TABLE_NAMES[1]+" t1,"+DatabaseHandler.TABLE_NAMES[3]+" t2,"+DatabaseHandler.TABLE_NAMES[4]+" t3 where t1.user_id=t2.user_id and t2.trip_id like '"+trip_id+"' and t3.loc_id = "+loc_id;
		}
		openDatabase();
		ArrayList<UserData> data = new ArrayList<UserData>();
		Cursor cursor = db.rawQuery(listQuery, null);
        if(cursor.moveToFirst())
        {
        	do
        	{
        		UserData temp = new UserData();
        		
        		temp.user_id = cursor.getInt(0);
        		temp.name = cursor.getString(1);
        		temp.phno = cursor.getString(2);
        		temp.distance = cursor.getDouble(3);
        		temp.pay = cursor.getDouble(4);
        		temp.loc = cursor.getString(5);
        		
        		data.add(temp);
        	}
        	while(cursor.moveToNext());
        }
        Toast.makeText(context, "Users: "+data.size(), Toast.LENGTH_SHORT).show();
        return data;
	}
	
	public ArrayList<String> getAllCrossedLocDataForATrip(String trip_id, String loc_id)
	{
		String listQuery = "select t1.loc_name from "+DatabaseHandler.TABLE_NAMES[2]+" t1 where t1.trip_id like '"+trip_id+"' and CAST(t1.loc_id AS NUMBER) < "+loc_id;
		openDatabase();
		ArrayList<String> data = new ArrayList<String>();
		try
		{
			Cursor cursor = db.rawQuery(listQuery, null);
			if(cursor.moveToFirst())
	        {
	        	do
	        	{
	        		data.add(new String(cursor.getString(0)));
	        	}
	        	while(cursor.moveToNext());
	        }
		}
		catch (Exception e)
		{
			Log.i("Crossed exception", e.toString());
		}
        return data;
	}
	
	public ArrayList<LocationData> getAllLocDataForATrip(String trip_id)
	{
		String listQuery = "select * from "+DatabaseHandler.TABLE_NAMES[2]+" t1 where t1.trip_id like '"+trip_id+"'";
		openDatabase();
		ArrayList<LocationData> data = new ArrayList<LocationData>();
		Cursor cursor = db.rawQuery(listQuery, null);
        if(cursor.moveToFirst())
        {
        	do
        	{
        		LocationData temp = new LocationData();
        		
        		temp.dist = cursor.getDouble(3);
        		temp.loc_id = cursor.getInt(1);
        		temp.name = cursor.getString(2);	//cursor.getString(2)
        		temp.tag = cursor.getString(4);
        		temp.price = cursor.getDouble(5);
        		temp.prev_price = cursor.getDouble(6);
        		temp.loc = new LatLng(cursor.getDouble(7), cursor.getDouble(8));
        		temp.comment = cursor.getString(9); 		
        		temp.crossed = getAllCrossedLocDataForATrip(trip_id, temp.loc_id+"");
        		temp.user = getUsersForATripOrLoc(trip_id, temp.loc_id, true);
               		
        		data.add(temp);
        	}
        	while(cursor.moveToNext());
        }
        Toast.makeText(context, "Locations: "+data.size(), Toast.LENGTH_SHORT).show();
        return data;
	}
	
	public void readFromDatabase_Trip_User()
	{
		String listQuery = "select * from trip_user";
		openDatabase();
		ArrayList<LocationData> data = new ArrayList<LocationData>();
		Cursor cursor = db.rawQuery(listQuery, null);
        if(cursor.moveToFirst())
        {
        	do
        	{
        		String sre = "fdgfdgf";
        		sre = "hfsuhg";
        	}
        	while(cursor.moveToNext());
        }
        Toast.makeText(context, "Locations: "+data.size(), Toast.LENGTH_SHORT).show();
	}
	
	public void readFromDatabase_User_Data()
	{
		String listQuery = "select * from user_data";
		openDatabase();
		ArrayList<LocationData> data = new ArrayList<LocationData>();
		Cursor cursor = db.rawQuery(listQuery, null);
        if(cursor.moveToFirst())
        {
        	do
        	{
        		String sre = "fdgfdgf";
        		sre = "hfsuhg";
        	}
        	while(cursor.moveToNext());
        }
        Toast.makeText(context, "Locations: "+data.size(), Toast.LENGTH_SHORT).show();
	}
	
	public void readFromDatabase_Location_Data()
	{
		String listQuery = "select * from location_data";
		openDatabase();
		ArrayList<LocationData> data = new ArrayList<LocationData>();
		Cursor cursor = db.rawQuery(listQuery, null);
        if(cursor.moveToFirst())
        {
        	do
        	{
        		String sre = "fdgfdgf";
        		sre = "hfsuhg";
        	}
        	while(cursor.moveToNext());
        }
        Toast.makeText(context, "Locations: "+data.size(), Toast.LENGTH_SHORT).show();
	}
}
