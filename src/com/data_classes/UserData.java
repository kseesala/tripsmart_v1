package com.data_classes;

import android.os.Parcel;
import android.os.Parcelable;

public class UserData implements Parcelable
{
	public String name = "";
	public String phno = "1234567890";
	public double distance = 0;
	public int check = 1;	//1 for working, 0 for pause
	//public Location loc_drop;
	public double pay;
	public String loc;
	public int user_id;
	
	public UserData()
	{
		check = 1;
	}
	
	public UserData(String a,String b)
	{
		name = a;
		phno = b;
	}

	public UserData(String name, String loc, int user_id)
	{
		this.name = name;
		this.loc = loc;
		this.user_id = user_id;
	}
	
	public UserData(Parcel source) 
	{
		readFromParcel(source);
	}

	@Override
	public int describeContents() 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void readFromParcel(Parcel in)
	{
		name = in.readString();
		phno = in.readString();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		// TODO Auto-generated method stub
		dest.writeString(name);
		dest.writeString(phno);
		dest.writeDouble(distance);
	}
	
	public static final Parcelable.Creator<UserData> CREATOR = new Parcelable.Creator<UserData>() 
	{

		@Override
		public UserData createFromParcel(Parcel source) 
		{
			// TODO Auto-generated method stub
			return new UserData(source);
		}

		@Override
		public UserData[] newArray(int size) 
		{
			// TODO Auto-generated method stub
			return new UserData[size];
		}
		
	};
}
