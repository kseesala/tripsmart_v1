
package com.data_classes;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

public class LocationData implements Comparable<LocationData>
{
	public int loc_id = 0;
	public String name = "";
	public double dist = 0;
	public String tag = "";	//checkpoint,gas_station
	public double price = 0;	//Price at current gas station
	public double prev_price = 0;	//Price at previous gas station
	public LatLng loc = new LatLng(0, 0);
	public String comment = "";	//If checkpoint then refers to username. If gas station then refers to gas station name.
	
	public ArrayList<UserData> user;
	
	public ArrayList<String> crossed;
	
	public LocationData()
	{
		
	}
	
	public LocationData(String name, double dist, String tag, double price, LatLng loc)
	{
		this.name = name;
		this.dist = dist;
		this.tag = tag;
		this.price = price;
		this.loc = loc;
		crossed = new ArrayList<String>();
		user = new ArrayList<UserData>();
	}

	@Override
	public int compareTo(LocationData another) 
	{
		if(dist > another.dist)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
}
