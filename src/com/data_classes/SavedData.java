package com.data_classes;

import java.util.ArrayList;

public class SavedData 
{
	public ArrayList<UserData> userdata;
	public ArrayList<LocationData> locdata;
	public String URI;
	public double mpg;
	public double tank;
	
	public SavedData(ArrayList<UserData> userdata, ArrayList<LocationData> locdata, String URI, double mpg, double tank)
	{
		this.userdata = userdata;
		this.locdata = locdata;
		this.URI = URI;	
		this.mpg = mpg;
		this.tank = tank;
	}
	
}
