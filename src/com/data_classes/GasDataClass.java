package com.data_classes;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

public class GasDataClass 
{	
	public double price;
	public String date;
	public String address, city, region, country, zip;
	public int diesel;
	public LatLng location;
	public String station;
	public String distance = "";	
	public ArrayList<LatLng> points = new ArrayList<LatLng>();

	public String last_updated = "";
	public String gas_station_id = "";
	
	public double deviation = 1000;
	public double total_cost = 1000;
	
	public GasDataClass(double price, String date, String address, String city, String region, String country, String zip, int diesel,double latitude,double longitude, String station, String distance, String last_updated, String gas_station_id)
	{
		this.price = price;
		this.date = date;
		this.address = address;
		this.city = city;
		this.region = region;
		this.country = country;
		this.zip = zip;
		this.diesel = diesel;
		this.location = new LatLng(latitude, longitude);
		this.station = station;
		this.distance = distance;
		this.last_updated = last_updated;
		this.gas_station_id = gas_station_id;
	}
}
