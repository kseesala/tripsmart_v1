package com.application.tripsmart;

import android.app.Application;

import com.bugsense.trace.BugSenseHandler;

public class MainApplication extends Application
{
	@Override
	public void onCreate()
	{
		super.onCreate();
		
		//BugTracker
		BugSenseHandler.initAndStartSession(this, "18d10802");
	}
}
