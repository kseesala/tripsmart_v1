package com.custom_textview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextView_1 extends TextView 
{		
	String font_type = "";
	
	public TextView_1(Context context, AttributeSet attrs, int defStyle) 
	{
        super(context, attrs, defStyle);
        init();
    }

    public TextView_1(Context context, AttributeSet attrs) 
    {
        super(context, attrs);
        init();
    }

    public TextView_1(Context context) 
    {
        super(context);
        init();
    }

    private void init() 
    {
        if (!isInEditMode()) 
        {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/stryde_regular.otf");
            setTypeface(tf);
        }
    }
    
    @Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
	}
}
