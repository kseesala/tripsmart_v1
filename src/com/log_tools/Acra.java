/*
 * Whenever application crashes, this application sends log data to bugsense
 */

package com.log_tools;

import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

import com.application.tripsmart.R;

@ReportsCrashes(
	//formKey="0eb687c39c3710182c72ea2f3a54ef36",
	formKey = "",
	formUri = "http://www.bugsense.com/api/acra?api_key=18d10802",
	mode = ReportingInteractionMode.DIALOG,
	//resToastText = R.string.crash_toast_text, // optional, displayed as soon as the crash occurs, before collecting data which can take a few seconds
	resDialogText = R.string.crash_dialog_text,
	resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
	resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
	resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, // optional. when defined, adds a user text field input with this text resource as a label
	resDialogOkToast = R.string.crash_dialog_ok_toast // optional. displays a Toast message when the user accepts to send a report.
)

public class Acra extends Application 
{
	@Override
	public void onCreate()
	{
		super.onCreate();
		
		// The following line triggers the initialization of ACRA
//		ACRA.init(this);
//		ACRA.getErrorReporter().setReportSender(new HockeySender());
		
		
//		BugSenseHandler.initAndStartSession(this, "18d10802");
//		HashMap<String, String> extras = new HashMap<String, String>();
//		extras.put("level", "second level");
//		extras.put("difficulty", "impossibruuu");
//		BugSenseHandler.addCrashExtraMap(extras);
		
//		CrashManager.register(this, "0eb687c39c3710182c72ea2f3a54ef36");
//		checkForCrashes();
	}	
}
