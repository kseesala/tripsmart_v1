package com.custom_dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.application.tripsmart.R;
import com.gas_module.AsyncTask_PostData;

public class UpdateGasDialog extends Dialog 
{

	String type,price;
	Spinner sp0;
	EditText ed0;
	Button b0,b1;
	Context context;
	String[] array_spinner = {"Regular","Midgrade","Premium"};
	String stationid;
	
	public UpdateGasDialog(Context context, String stationid) 
	{
		super(context);
		this.context = context;
		this.stationid = stationid;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_update_gas);
		
		sp0 = (Spinner)findViewById(R.id.type);
		ed0 = (EditText)findViewById(R.id.price);
		b0 = (Button)findViewById(R.id.cancel);
		b1 = (Button)findViewById(R.id.ok);

		ArrayAdapter adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, array_spinner);
		sp0.setAdapter(adapter);
		
		b0.setOnClickListener(new View.OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				dismiss();
			}
			
		});
		
		b1.setOnClickListener(new View.OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				
				String type = sp0.getSelectedItem().toString().substring(0, 3).toLowerCase();
				String price = ed0.getText().toString();
				
				Log.i("price", price);
				
				if(!price.equals(""))
				{
					if(Double.parseDouble(price)>6)
					{
						Toast.makeText(context, "The price seems unreasonable, Please check your value.", Toast.LENGTH_LONG).show();
					}
					else
					{
						AsyncTask_PostData post = new AsyncTask_PostData(context);
						try
						{
							if(post.execute(price,type,stationid).get() == true)
							{
								Toast.makeText(context, "Data has been updated successfully", Toast.LENGTH_LONG).show();
							}
							else
							{
								Toast.makeText(context, "Error updating data, Please try again later", Toast.LENGTH_LONG).show();
							}
						}
						catch (Exception e)
						{
							Log.e("UpdateGasDialog", e.toString());
						}
						dismiss();
					}
					// TODO Auto-generated method stub
				}
				else
				{
					ed0.setError("The price cannot be empty.");
				}
			}
		});
		
	}

}
