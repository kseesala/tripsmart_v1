/*
 * Custom Progress Dialog
 */

package com.custom_dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.application.tripsmart.R;

public class ProgressDialog_1 extends Dialog 
{
	TextView tv1;
	String text = "";
	int width = 0;
	int height = 0;
	
	public ProgressDialog_1(Context context, String text, int width, int height) 
	{
		super(context);
		this.text = text;
		this.width = width;
		this.height = height;
		this.setCancelable(false);
		this.setCanceledOnTouchOutside(false);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_progress1);
		
		width = width * 75/100;
		height = height * 20/100;
		getWindow().setLayout(width, height);
		
		tv1 = (TextView)findViewById(R.id.textView1);
		tv1.setText(text);
	}
}
