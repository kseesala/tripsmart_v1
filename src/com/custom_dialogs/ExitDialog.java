package com.custom_dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;

import com.application.tripsmart.R;

public class ExitDialog extends Dialog implements android.view.View.OnClickListener 
{
	public Context context;
	public Dialog d;
	public ImageButton yes,no;
	
	public ExitDialog(Context context) 
	{
		super(context);
		this.context = context;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_exit);
		
		yes = (ImageButton)findViewById(R.id.imageButton1);
		no = (ImageButton)findViewById(R.id.imageButton2);
		
		yes.setOnClickListener(this);
		no.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		// TODO Auto-generated method stub
		Activity act = (Activity)context;
		switch (v.getId()) 
		{
		case R.id.imageButton1:
			act.finish();
			break;
		case R.id.imageButton2:
			Toast.makeText(context, "Ok I won't do that", Toast.LENGTH_SHORT).show();
			dismiss();
			break;
		default:
			break;
		}
	}
}
