package com.custom_dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.application.tripsmart.R;

public class Sort_By_Dialog extends Dialog 
{
	String text = "";
	int width = 0;
	int height = 0;
	
	public Sort_By_Dialog(Context context, int width, int height) 
	{
		super(context);
		this.width = width;
		this.height = height;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_gas_sort_by);
		
		width = width * 75/100;
		height = height * 20/100;
		getWindow().setLayout(width, height);
		
	}
}
