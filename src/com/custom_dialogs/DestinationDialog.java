package com.custom_dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.application.tripsmart.R;

public class DestinationDialog extends Dialog 
{
	int width = 0;
	int height = 0;
	
	public DestinationDialog(Context context, int width, int height) 
	{
		super(context);
		// TODO Auto-generated constructor stub
		this.width = width;
		this.height = height;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_skeleton_destination);
		
//		width = width * 75/100;
//		height = height * 20/100;
		getWindow().setLayout(width, height);
		
	}
	
}
